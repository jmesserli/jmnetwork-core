/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.arguments;

import java.util.HashMap;

/**
 * A simple Argument Parser.
 * <p/>
 * Give it all your Arguments and then you can get the values the users set via {@link #getArgument(String)}, {@link #getBoolean(String)} or {@link #getAllArgumentsAndValues()}
 */
public class ArgumentParser {
    private HashMap<String, String> argValues = new HashMap<>();
    String[] arguments;
    String prefix;

    /**
     * Constructor
     *
     * @param args        The arguments array from main(String[] args)
     * @param args_prefix The prefix which should be used to prefix the argument names
     */
    public ArgumentParser(String[] args, String args_prefix) {
        arguments = args;
        prefix = args_prefix;
        parseArguments();
    }

    private void parseArguments() {
        String nextArgument = null;

        for (int _cnt = 0; _cnt < arguments.length; _cnt++) {
            if (nextArgument != null && !arguments[_cnt].startsWith(prefix)) {
                argValues.put(nextArgument, arguments[_cnt]);
                nextArgument = null;
            } else if (nextArgument != null && arguments[_cnt].startsWith(prefix)) {
                argValues.put(nextArgument, "true");
                nextArgument = null;
            } else {
                if (!arguments[_cnt].startsWith(prefix)) continue;

                nextArgument = arguments[_cnt].substring(prefix.length());
            }
        }
    }

    /**
     * @param argumentName name of the argument to get the value from
     * @return the value of the argument or null if the specified argument was not specified
     */
    public String getArgument(String argumentName) {
        return argValues.get(argumentName);
    }

    /**
     * Gets the Boolean value of an argument
     *
     * @param argumentName Name of the argument to get the boolean value from
     * @return The Boolean value of the specified argument or <b>null</b> if the argument doesn't exist or is not a boolean
     */
    public Boolean getBoolean(String argumentName) {
        if (!isArgumentSet(argumentName)) return null;
        String arg_value = getArgument(argumentName);
        return arg_value.equals("true") ? true : arg_value.equals("false") ? false : null;
    }

    /**
     * Is a specific argument set?
     *
     * @param argumentName The argument to check
     * @return If the specified argument is != null
     */
    public boolean isArgumentSet(String argumentName) {
        return getArgument(argumentName) != null;
    }

    /**
     * Get all set arguments and their values
     *
     * @return A {@link java.util.HashMap} with all the arguments and values
     */
    public HashMap<String, String> getAllArgumentsAndValues() {
        return argValues;
    }
}
