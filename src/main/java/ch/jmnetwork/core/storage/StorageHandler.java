/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.storage;

import ch.jmnetwork.core.localization.JMNetworkLocalization;
import ch.jmnetwork.core.localization.Localization;
import ch.jmnetwork.core.tools.codec.EnDeCodingUtils;
import ch.jmnetwork.core.tools.xml.BetterNodeList;
import ch.jmnetwork.core.tools.xml.XmlUtils;
import org.w3c.dom.*;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import javax.xml.xpath.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * User: joel / Date: 05.01.14 / Time: 14:44
 */
public class StorageHandler {
    File xmlFile = null;
    Document document;
    Element rootElement;
    ArrayList<StorageObject> properties = new ArrayList<>();
    XPathFactory xPathfactory = XPathFactory.newInstance();
    XPath xpath = xPathfactory.newXPath();
    javax.xml.xpath.XPathExpression pathExpression;
    // Localization
    Localization localization = JMNetworkLocalization.getStorageLocalization();
    String attribute_name_name = localization.getLocalizedString("storage.attributes.name.name");
    String attribute_class_name = localization.getLocalizedString("storage.attributes.class.name");
    String element_value_name = localization.getLocalizedString("storage.elements.value.name");
    String rootelement_name = localization.getLocalizedString("storage.elements.rootelement.name");
    String storageobject_name = localization.getLocalizedString("storage.elements.storageobject.name");
    private ArrayList<String> propertyNameList = new ArrayList<>();


    public StorageHandler(File file) {
        boolean exists = true;
        xmlFile = file;

        if (!xmlFile.exists()) {
            try {
                if (!xmlFile.createNewFile())
                    throw new RuntimeException("Cannot create file " + xmlFile.getCanonicalPath());
                exists = false;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (xmlFile.length() == 0) {
            exists = false;
        }
        if (exists) {
            document = XmlUtils.getXMLDocumentFromFile(file);
            rootElement = XmlUtils.getRootElement(document);
            loadStorage();
        } else {
            document = XmlUtils.getEmptyDocument(rootelement_name);
            rootElement = document.getDocumentElement();
        }
    }

    private void loadStorage() {
        NodeList child_list = XmlUtils.getAllElementsWithName(storageobject_name, rootElement);
        BetterNodeList b_nodelist = new BetterNodeList(child_list);
        for (Node e : b_nodelist) {
            try {
                NamedNodeMap attributes = e.getAttributes();
                Node attribute_name = attributes.getNamedItem(attribute_name_name);
                Node attribute_class = attributes.getNamedItem(attribute_class_name);
                Node value = XmlUtils.getFirstChildElementWithName(element_value_name, e);

                properties.add(new StorageObject<>(attribute_name.getTextContent(), value.getTextContent(), Class.forName(attribute_class.getTextContent())));
                propertyNameList.add(attribute_name.getTextContent());
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void setStringProperty(String name, String value) {
        setProperty(name, value, String.class);
    }

    public void setIntegerProperty(String name, int value) {
        setProperty(name, value, Integer.class);
    }

    public <T> void setProperty(String name, T value, Class<T> valueClass) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos;
        String serObjectString = null;
        try {
            oos = new ObjectOutputStream(bos);
            oos.writeObject(value);
        } catch (IOException e) {
            e.printStackTrace();
        }

        serObjectString = EnDeCodingUtils.getBase64EncodedString(bos.toByteArray());

        if (!propertyNameList.contains(name)) {
            Element property = document.createElement(storageobject_name);
            property.setAttribute(attribute_name_name, name);
            property.setAttribute(attribute_class_name, valueClass.getName());
            Element t_element = document.createElement(element_value_name);
            t_element.setTextContent(serObjectString);
            property.appendChild(t_element);

            rootElement.appendChild(property);
            properties.add(new StorageObject<>(name, serObjectString, valueClass));
            propertyNameList.add(name);
        } else {
            String query = "//" + storageobject_name + "[@" + attribute_name_name + "= '" + name + "']";
            try {
                pathExpression = xpath.compile(query);
            } catch (XPathExpressionException e) {
                e.printStackTrace();
            }
            Node e = null;
            try {
                e = (Node) pathExpression.evaluate(document, XPathConstants.NODE);
            } catch (XPathExpressionException e1) {
                e1.printStackTrace();
            }

            assert e != null;

            NamedNodeMap attribute_map = e.getAttributes();
            Node class_attribute = attribute_map.getNamedItem(attribute_class_name);

            class_attribute.setTextContent(valueClass.getName());
            Element val = document.createElement(element_value_name);
            val.setTextContent(serObjectString);

            int i = -1;
            for (int _cnt = 0; _cnt < properties.size(); _cnt++) {
                if (properties.get(_cnt).name.equals(name)) {
                    i = _cnt;
                    break;
                }
            }

            if (i != -1) properties.set(i, new StorageObject<>(name, serObjectString, valueClass));
        }
    }

    public StorageObject getStorageObjectByName(String name) {
        if (!propertyNameList.contains(name)) return null;
        for (StorageObject p : properties) {
            if (p.name.equals(name)) return p;
        }
        return null;
    }

    public <T> StorageObject<T> getStorageObjectByName(String name, Class<T> objectClass) {
        if (!propertyNameList.contains(name)) return null;
        for (StorageObject p : properties) {
            if (p.name.equals(name)) return (StorageObject<T>) p;
        }
        return null;
    }

    public void saveStorage() {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        assert transformer != null;
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(xmlFile);
        try {
            transformer.transform(source, result);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}