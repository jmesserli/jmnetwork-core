/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.storage;

import ch.jmnetwork.core.tools.codec.EnDeCodingUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;

public final class StorageObject<T> {

    public final String name, value;
    public final Class<T> type;
    public final T object;

    public StorageObject(String name, String value, Class<T> type) {
        this.name = name;
        this.value = value;
        this.type = type;
        object = getObject();
    }

    public Class<T> getObjectClass() {
        return type;
    }

    public T getObject() {
        if (object != null) return object;
        ByteArrayInputStream bis;
        ObjectInputStream ois;


        try {
            bis = new ByteArrayInputStream(EnDeCodingUtils.getBase64DecodedByteArray(value));
            ois = new ObjectInputStream(bis);

            return (T) ois.readObject();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getString() {
        if (type != String.class) throw new UnsupportedOperationException("Not a String property!");
        return (String) getObject();
    }

    public int getInteger() {
        if (type != Integer.class) throw new UnsupportedOperationException("Not an Integer property!");
        return (Integer) getObject();
    }

    @Override
    public boolean equals(Object obj) {
        StorageObject p = (StorageObject) obj;

        return p.name.equals(name) && p.value.equals(value) && p.type.equals(type);
    }
}