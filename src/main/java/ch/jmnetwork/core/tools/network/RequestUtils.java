/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.network;

import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.logging.LogLevel;

import java.io.*;
import java.net.*;
import java.util.HashMap;

public class RequestUtils {

    public static String doGet(String url, String... args) {
        if (args.length % 2 != 0) throw new IllegalArgumentException();

        HashMap<String, String> argu = new HashMap<>();
        int _cnt = 0;
        for (int i = 0; i < (args.length / 2); i++) {
            argu.put(args[_cnt], args[_cnt + 1]);
            _cnt += 2;
        }

        return doGet(url, argu);
    }

    public static String doGet(String url, HashMap<String, String> args) {
        return doGet(url, "UTF-8", args);
    }

    public static String doGet(String url, String charset, HashMap<String, String> args) {
        try {
            return doGet(new URL(url), charset, args);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String doGet(URL url, String charset, HashMap<String, String> args) {
        String params = buildParamString(args, charset);
        String finalurl = url.toString() + "?" + params;
        JMNetworkCore.logger.log(LogLevel.DEBUG, "[RQU] Full url: " + finalurl);
        URLConnection connection = null;
        try {
            connection = new URL(finalurl).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert connection != null;
        connection.setRequestProperty("Accept-Charset", charset);
        InputStream response = null;
        try {
            response = connection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return readAll(response);
    }

    public static String doPost(String url, String... args) {
        if (args.length % 2 != 0) throw new IllegalArgumentException();

        HashMap<String, String> argu = new HashMap<>();
        int _cnt = 0;
        for (int i = 0; i < (args.length / 2); i++) {
            argu.put(args[_cnt], args[_cnt + 1]);
            _cnt += 2;
        }

        return doPost(url, argu);
    }

    public static String doPost(String url, HashMap<String, String> args) {
        return doPost(url, "UTF-8", args);
    }

    public static String doPost(String url, String charset, HashMap<String, String> args) {
        try {
            return doPost(new URL(url), charset, args);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String doPost(URL url, String charset, HashMap<String, String> args) {
        String params = buildParamString(args, charset);
        JMNetworkCore.logger.log(LogLevel.DEBUG, "[RQU] All parameters: " + params);
        return doPost(url, charset, ContentType.FORM_URLENCODED_UTF8, params);
    }

    public static String doPost(URL url, String charset, ContentType contentType, String body) {
        return doPost(url, charset, contentType.cts, body);
    }

    public static String doPost(URL url, String charset, String contenttype, String body) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert connection != null;
        connection.setDoOutput(true);
        connection.setRequestProperty("Accept-Charset", charset);
        connection.setRequestProperty("Content-Type", contenttype);
        OutputStream stream = null;
        try {
            stream = connection.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            stream.write(body.getBytes(charset));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (connection.getResponseCode() == 200) return readAll(connection.getInputStream());
            else return readAll(connection.getErrorStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private static String buildParamString(final HashMap<String, String> map, String charset) {
        String returnstring = "";

        int _cnt = 0;
        for (String key : map.keySet()) {
            try {
                returnstring += key + "=" + URLEncoder.encode(map.get(key), charset) + (_cnt + 1 == map.keySet().size() ? "" : "&");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            _cnt++;
        }

        return returnstring;
    }

    private static String readAll(InputStream inputStream) {
        String ret = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        String temp;
        try {
            while ((temp = reader.readLine()) != null) {
                ret += temp;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    public enum ContentType {

        FORM_URLENCODED_UTF8("application/x-www-form-urlencoded;charset=UTF-8"), JSON("application/json");

        String cts;

        ContentType(String cts) {
            this.cts = cts;
        }
    }
}
