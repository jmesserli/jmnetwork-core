/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.network;

import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.logging.LogLevel;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Observable;

/**
 * The File Downloader Thread downloads files in the background.
 * You can get its status with the methods {@link #getDownloadedSize()}, {@link #getContentLength()} and {@link #isComplete()}
 * <p/>
 * You can implement Observer and then {@link #addObserver(java.util.Observer)} to wait for updates.
 * It will only send one update. That is when the download is finished.
 * The update sends the FileDownloaderThread itself.
 */
public class FileDownloaderThread extends Observable implements Runnable {
    private long downloadedSize = 0;
    private BufferedInputStream bis;
    private FileOutputStream fos;
    private boolean downloadComplete = false;
    private long contentLength;

    byte data[] = new byte[1024];
    int count;

    public FileDownloaderThread(BufferedInputStream inputstream, FileOutputStream outputStream, long contentLength) {
        bis = inputstream;
        fos = outputStream;
        this.contentLength = contentLength;
    }

    @Override
    public void run() {
        try {
            while ((count = bis.read(data, 0, 1024)) != -1) {
                fos.write(data, 0, count);
                downloadedSize += count;
            }
            finish();
        } catch (IOException e) {
            JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e, "[FDT] IOException while writing");
        } finally {
            if (bis != null && fos != null) {
                try {
                    bis.close();
                    fos.close();
                } catch (IOException e) {
                    JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e);
                }
            }
        }
    }

    private void finish() {
        downloadComplete = true;
        setChanged();
        notifyObservers(this);
    }

    public long getDownloadedSize() {

        return downloadedSize;
    }

    public long getContentLength() {
        return contentLength;
    }

    public boolean isComplete() {

        return downloadComplete;
    }
}
