/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.network;

import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.logging.LogLevel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Observer;

/**
 * User: joel / Date: 02.01.14 / Time: 23:30
 */
public class NetworkUtils {


    public static long contentLengthToKiloBytes(long contentLength) {
        return contentLengthToKiloBytes((float) contentLength);
    }

    public static long contentLengthToKiloBytes(float contentLength) {
        return Math.round(contentLength / 1024F);
    }

    public static File simpleDownload(URL url, String saveLocation) {
        return simpleDownload(url.toString(), saveLocation);
    }

    public static File simpleDownload(String url, String saveLocation) {
        long timeStart = System.currentTimeMillis();
        FileDownloaderThread dlt = saveURLto(url, saveLocation);

        while (!dlt.isComplete()) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long timeEnd = System.currentTimeMillis();

        JMNetworkCore.logger.log(LogLevel.DEBUG, "Download Completed in " + Math.round((timeEnd - timeStart) / 1000F * 100F) / 100F + "s");

        return new File(saveLocation);
    }

    public static FileDownloaderThread saveURLto(String URL, String saveLocation) {
        return saveURLto(URL, saveLocation, null);
    }

    /**
     * This is used to get a File Downloader Thread which will download the requested file in the background.
     *
     * @param URL          The URL of what you want to download
     * @param saveLocation Where the file should be saved (File Name)
     * @param listener     a {@link java.util.Observer} to notify when the download finished
     * @return File Downloader Thread
     * @see FileDownloaderThread
     */
    public static FileDownloaderThread saveURLto(String URL, String saveLocation, Observer listener) {
        java.net.URL website;
        HttpURLConnection httpuc = null;

        FileOutputStream fos;
        File f = new File(saveLocation);

        FileDownloaderThread fd = null;

        long contentLength = 0;

        try {
            website = new URL(URL);
            try {
                httpuc = (HttpURLConnection) website.openConnection();
                contentLength = httpuc.getContentLengthLong();
            } catch (IOException e1) {
                JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e1, "Cannot open connection to " + URL);
            }
            JMNetworkCore.logger.log(LogLevel.DEBUG, "[Network Utils] Creating directories and files...");
            if (!f.exists()) {
                try {
                    File parentFile = null;
                    if (f.getParentFile() != null) {
                        parentFile = f.getParentFile();
                    }
                    if (!(parentFile == null)) {
                        parentFile.mkdirs();
                    }
                } catch (Exception e) {
                    JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e, "[Network Utils] Error while creating files");
                }
                f.createNewFile();
            }
            fos = new FileOutputStream(saveLocation);

            fd = new FileDownloaderThread(new BufferedInputStream(httpuc.getInputStream()), fos, contentLength);
            if (listener != null) fd.addObserver(listener);
            new Thread(fd).start();
        } catch (Exception e) {
            JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e, "[Network Utils] Error while downloading from " + URL);
        }

        return fd;
    }
}
