/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.math;

import ch.jmnetwork.core.tools.code.Functions;
import ch.jmnetwork.core.tools.list.ListUtils;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * User: joel / Date: 31.12.13 / Time: 22:29
 */
public class MathUtils {

    public static ArrayList<Long> getPrimeNumbersOf(long from) {
        ArrayList<Long> foundPrimes = new ArrayList<>();

        int i;
        long last = from;

        for (i = 2; i <= last; i++) {
            if (last % i == 0L) {
                foundPrimes.add((long) i);

                last /= i;
                i = 1;
            }
        }

        if (last != 1L) {
            foundPrimes.add(last);
        }

        return foundPrimes;
    }

    public static long addAllInt(ArrayList<Integer> toAdd) {
        return addAllLong(Functions.convert(toAdd, Functions.INT_ARRAYLIST_TO_LONG_ARRAYLIST));
    }

    public static long addAllLong(ArrayList<Long> toAdd) {
        long ret = 0L;

        for (Long l : toAdd) {
            ret += l;
        }

        return ret;
    }

    public static long multAll(ArrayList<Long> toMult) {
        if (toMult.size() == 0) return 0L;
        long num = toMult.get(0);

        for (int _cnt = 1; _cnt < toMult.size(); _cnt++) {
            num *= toMult.get(_cnt);
        }

        return num;
    }

    public static long getGreatestCommonFactor(Long... from) {
        ArrayList<Long> t_list = getPrimeNumbersOf(from[0]);

        for (int _cnt = 1; _cnt < from.length; _cnt++) {
            t_list = ListUtils.getMatchingItems(t_list, MathUtils.getPrimeNumbersOf(from[_cnt]));
        }

        long temp = multAll(t_list);
        return temp == 0 ? 1 : temp;
    }

    public static long getLeastCommonMultiple(Long... from) {
        long r_val = getLeastCommonMultiple(from[0], from[1]);

        for (int _cnt = 2; _cnt < from.length; _cnt++) {
            r_val = getLeastCommonMultiple(r_val, from[_cnt]);
        }

        return r_val;
    }

    public static long getLeastCommonMultiple(Long from, Long from2) {
        return Math.abs(from * from2) / getGreatestCommonFactor(from, from2);
    }

    public static BigInteger factorial(long l) {
        BigInteger l2 = new BigInteger(l + "");

        for (long _cnt = l - 1; _cnt > 0; _cnt--) {
            l2 = l2.multiply(new BigInteger(_cnt + ""));
        }

        return l2;
    }

    public static double round(double d, int positions) {
        double factor = Math.pow(10, positions);

        double temp = d * factor;
        temp = Math.round(temp);
        temp /= (double) factor;

        return temp;
    }
}