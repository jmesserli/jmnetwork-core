/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.list;

import ch.jmnetwork.core.tools.iterate.SimpleCountIterator;
import ch.jmnetwork.core.tools.string.StringUtils;

import java.util.ArrayList;

/**
 * User: joel / Date: 25.01.14 / Time: 09:01
 */
public class MultilistManager<T> {

    private ArrayList<ArrayList<T>> lists = new ArrayList<>();
    private Class<T> myGenericType;

    public MultilistManager(ArrayList<T>... arrayLists) {
        this(null, arrayLists);
    }

    @SafeVarargs
    public MultilistManager(Class<T> myGenericType, ArrayList<T>... arrayLists) {
        this.myGenericType = myGenericType;
        for (ArrayList<T> list : arrayLists) {
            lists.add(list);
        }
    }

    public boolean put(final T... values) {
        if (values.length < lists.size()) return false;

        SimpleCountIterator countIterator = new SimpleCountIterator(0, lists.size() - 1) {
            @Override
            public void doIteration() {
                lists.get(_cnt).add(values[_cnt]);
            }
        };
        countIterator.iterate();

        return true;
    }

    public ArrayList<T> getLine(int index) {
        ArrayList<T> r_list = new ArrayList<>();

        for (ArrayList<T> list : lists) {
            r_list.add(list.get(index));
        }

        return r_list;
    }

    public ArrayList<T> getList(int index) {
        return lists.get(index);
    }

    public String toColumnizedString() {
        if (myGenericType != null && myGenericType == String.class) {
            ArrayList<String>[] arrayLists = new ArrayList[lists.size()];

            for (int i = 0; i < lists.size(); i++)
                arrayLists[i] = (ArrayList<String>) lists.get(i);

            return StringUtils.getColumnizedString(arrayLists);
        }
        return null;
    }
}
