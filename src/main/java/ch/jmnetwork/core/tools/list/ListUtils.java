/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.list;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * User: joel / Date: 30.12.13 / Time: 16:39
 */
public class ListUtils {


    public static <T> ArrayList<T> getListFromIndexInArraysInList(ArrayList<T[]> list, int index) {
        ArrayList<T> t_list = new ArrayList<>();

        for (T[] element_a : list) {
            t_list.add(element_a[index]);
        }

        return t_list;
    }

    @SafeVarargs
    public static <T> ArrayList<T[]> getListOfMultipleArrays(T[]... arrays) {
        ArrayList<T[]> t_list = new ArrayList<>();

        for (T[] array : arrays) t_list.add(array);

        return t_list;
    }

    public static <T> ArrayList<T> getListFrom(T from) {
        ArrayList<T> list = new ArrayList<>();
        list.add(from);
        return list;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public static <T> ArrayList<T[]> getListWithArraysOfLists(Class<T> arrayClass, ArrayList<T>... list) {
        ArrayList<T[]> t_list = new ArrayList<>();
        int list_size = list.length;

        for (int _cnt_ = 0; _cnt_ < list[0].size(); _cnt_++) {
            T[] array = (T[]) Array.newInstance(arrayClass, list_size);
            for (int _cnt = 0; _cnt < list_size; _cnt++)
                array[_cnt] = list[_cnt].get(_cnt_);
            t_list.add(array);
        }

        return t_list;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public static ArrayList<String[]> getStringListWithArraysOfLists(ArrayList<String>... list) {
        ArrayList<String[]> t_list = new ArrayList<>();
        int list_size = list.length;

        for (int _cnt_ = 0; _cnt_ < list[0].size(); _cnt_++) {
            String[] array = new String[list_size];
            for (int _cnt = 0; _cnt < list_size; _cnt++)
                array[_cnt] = list[_cnt].get(_cnt_);
            t_list.add(array);
        }

        return t_list;
    }

    public static <T> ArrayList<T> addItemToTopOfList(ArrayList<T> list, T itemToPrepend) {
        ArrayList<T> ret_list = new ArrayList<>();

        ret_list.add(itemToPrepend);
        for (T element : list) {
            ret_list.add(element);
        }
        return ret_list;
    }

    public static <T> ArrayList<String> listToStringList(ArrayList<T> list) {
        ArrayList<String> t_list = new ArrayList<>();

        for (T e : list) t_list.add(e.toString());

        return t_list;
    }

    public static <T> ArrayList<T> getMatchingItems(ArrayList<T> list1, ArrayList<T> list2) {
        ArrayList<T> r_list = new ArrayList<>();
        ArrayList<Integer> ignore_1 = new ArrayList<>(), ignore_2 = new ArrayList<>();

        int i1 = 0, i2;
        for (T element : list1) {
            i2 = 0;
            for (T element2 : list2) {
                if (element.equals(element2) && !ignore_1.contains(i1) && !ignore_2.contains(i2)) {
                    r_list.add(element);
                    ignore_1.add(i1);
                    ignore_2.add(i2);
                    break;
                }
                i2++;
            }
            i1++;
        }

        return r_list;
    }

    public static <T> ArrayList<HashMap<T, Long>> countAll(ArrayList<T>... lists) {
        ArrayList<HashMap<T, Long>> r_list = new ArrayList<>();

        for (ArrayList<T> list : lists) {
            HashMap<T, Long> tempMap = new HashMap<>();
            for (T e : list) {
                if (tempMap.containsKey(e)) tempMap.put(e, tempMap.get(e) + 1L);
                else tempMap.put(e, 1L);
            }
            r_list.add(tempMap);
        }

        return r_list;
    }

    public static <T> HashMap<T, Long> count(ArrayList<T> list) {

        HashMap<T, Long> tempMap = new HashMap<>();

        for (T e : list) {
            if (tempMap.containsKey(e)) tempMap.put(e, tempMap.get(e) + 1L);
            else tempMap.put(e, 1L);
        }

        return tempMap;
    }

    @SafeVarargs
    public static <T> ArrayList<T> addAllItems(ArrayList<T>... add) {
        ArrayList<T> t_list = new ArrayList<>();

        for (ArrayList<T> list : add) {
            for (T element : list) {
                t_list.add(element);
            }
        }

        return t_list;
    }

    public static <T> ArrayList<T> addAll(T... add) {
        ArrayList<T> t_list = new ArrayList<>();

        for (T e : add) {
            t_list.add(e);
        }

        return t_list;
    }
}

