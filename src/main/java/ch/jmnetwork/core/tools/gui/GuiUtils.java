/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.gui;

import ch.jmnetwork.core.annotations.TODO;
import ch.jmnetwork.core.gui.JDrawingPane;
import ch.jmnetwork.core.gui.JGUIManager;
import ch.jmnetwork.core.tools.code.MultitypeBag;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

public class GuiUtils {

    /**
     * Get a GradientPaint which looks like you want on a component with x/y != 0
     *
     * @param comp_x Component x position
     * @param comp_y Component y position
     * @return GradientPaint relative to comp_x, comp_y
     */
    public static GradientPaint getRelativeGradientPaint(double x1, double y1, Color c1, double x2, double y2, Color c2, float comp_x, float comp_y) {
        return new GradientPaint(new Point2D.Double(x1 + comp_x, y1 + comp_y), c1, new Point2D.Double(x2 + comp_x, y2 + comp_y), c2);
    }

    /**
     * Get a GradientPaint which looks like you want on a component with x/y != 0
     *
     * @param component_x Component x position
     * @param component_y Component y position
     * @return GradientPaint relative to component_x, component_y
     */
    public static GradientPaint getRelativeGradientPaint(GradientPaint gradientPaint, int component_x, int component_y) {
        Point2D p1 = gradientPaint.getPoint1(), p2 = gradientPaint.getPoint2();
        Color c1 = gradientPaint.getColor1(), c2 = gradientPaint.getColor2();

        return getRelativeGradientPaint(p1.getX(), p1.getY(), c1, p2.getX(), p2.getY(), c2, component_x, component_y);
    }

    /**
     * Creates a simple JFrame setup to do JGUI stuff.
     *
     * @param xsize x size of the window
     * @param ysize y size of the window
     * @return A {@link ch.jmnetwork.core.tools.code.MultitypeBag} with a {@link javax.swing.JFrame} and a {@link ch.jmnetwork.core.gui.JGUIManager} ready to be used.
     */
    public static MultitypeBag<JFrame, JGUIManager> getSimpleGUI(int xsize, int ysize) {
        MultitypeBag<JFrame, JGUIManager> bag1;

        JGUIManager manager = new JGUIManager(xsize, ysize);
        JDrawingPane pane = new JDrawingPane(manager);
        JFrame frame = new JFrame();
        frame.setLocationRelativeTo(null);
        frame.setContentPane(pane);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        bag1 = new MultitypeBag<>(frame, manager);

        return bag1;
    }

    @TODO(what = "make work!")
    public static Point getTextPos(String text, Graphics2D graphics, double angle, Point textPosition, boolean inside) {

        Rectangle2D stringBounds = graphics.getFontMetrics().getStringBounds(text, graphics);

        Point2D.Double center = new Point2D.Double(stringBounds.getCenterX(), stringBounds.getCenterY());
        Point2D.Double helppoint = new Point2D.Double(center.x, stringBounds.getY() / 2);

        AffineTransform transform = AffineTransform.getRotateInstance(Math.toRadians(angle), center.getX(), center.getY());

        transform.transform(helppoint, helppoint);

        double lowerLX = stringBounds.getX(), lowerLY = stringBounds.getY() + stringBounds.getHeight();
        double deltaX = helppoint.getX() - lowerLX, deltaY = helppoint.getY() - lowerLY;

        Point thePoint = new Point((int) Math.round(textPosition.getX() - deltaX), (int) Math.round(textPosition.getY() - deltaY));

        return thePoint;
    }
}
