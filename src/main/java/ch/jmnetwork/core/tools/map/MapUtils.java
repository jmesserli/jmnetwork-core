/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.map;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * User: joel / Date: 22.01.14 / Time: 18:07
 */
public class MapUtils {

    public static <T, E> HashMap<T, E> addAllMaps(ArrayList<HashMap<T, E>> maplist) {
        HashMap<T, E> r_map = new HashMap<>();

        for (HashMap<T, E> map : maplist) {
            for (T e : map.keySet()) {
                r_map.put(e, map.get(e));
            }
        }

        return r_map;
    }
}
