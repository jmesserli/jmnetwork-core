/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.array;

import ch.jmnetwork.core.tools.code.Function;
import ch.jmnetwork.core.tools.code.Functions;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class ArrayUtils {

    public static <T> ArrayList<T> arrayToList(T[] array) {
        ArrayList<T> list = new ArrayList<>();
        if (array == null) return list;

        Collections.addAll(list, array);

        return list;
    }

    @SafeVarargs
    public static <T> String join(String glue, Function<T, String> function, T... pieces) {
        if (pieces.length == 0) return "null";
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < pieces.length; i++) {
            stringBuilder.append(function.map(pieces[i])).append(i + 1 != pieces.length ? glue : "");
        }

        return stringBuilder.toString();
    }

    public static String join(String glue, Object... pieces) {
        return join(glue, Functions.OBJECT_TO_STRING, pieces);
    }

    public static <T> boolean contains(T[] array, T toFind) {
        for (T element : array) {
            if (element.equals(toFind)) return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] arrayWithout(T[] array, Class<T> arrayClass, Integer... withoutIndexes) {
        if ((array.length - withoutIndexes.length) < 0)
            throw new IllegalArgumentException("You want to remove more than there exists!");
        else if ((array.length - withoutIndexes.length) == 0) return (T[]) Array.newInstance(arrayClass, 0);

        ArrayList<T> templist = new ArrayList<>();

        int _cnt = 0;
        for (T element : array) {
            if (!contains(withoutIndexes, _cnt)) {
                templist.add(element);
            }
            _cnt++;
        }

        return templist.toArray((T[]) Array.newInstance(arrayClass, templist.size()));
    }
}
