/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.bitop;

public class BitUtils {

    public static int[] intFlags = new int[31];

    static {
        for (int i = 0; i < intFlags.length; i++) {
            intFlags[i] = 1 << i;
        }
    }

    public static int setFlag(int flags, int flagToSet) {
        return flags | intFlags[flagToSet];
    }

    public static int unsetFlag(int flags, int flagToUnset) {
        return flags & ~intFlags[flagToUnset];
    }

    public static boolean isFlagSet(int flags, int flag) {
        return (flags & intFlags[flag]) == intFlags[flag];
    }

    public static int and(int flags, int flags_) {
        return flags & flags_;
    }

    public static int xor(int flags, int flags_) {
        return flags ^ flags_;
    }

    public static int or(int flags, int flags_) {
        return flags | flags_;
    }
}
