/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.zip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

public class ZipUtils {

    public static ZipFile getZipFileFromFile(String file) {
        return getZipFileFromFile(new File(file));
    }

    public static ZipFile getZipFileFromFile(File file) {
        try {
            return new ZipFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Enumeration<? extends ZipEntry> getAllEntries(ZipFile file) {
        return file.entries();
    }

    public static ZipEntry getEntry(ZipFile file, String entryName) {
        return file.getEntry(entryName);
    }

    public static void unzipAll(ZipFile file, File todir) {
        Enumeration<? extends ZipEntry> entries = file.entries();

        while (entries.hasMoreElements()) {
            ZipEntry en = entries.nextElement();
            unzip(file, en, new File(todir, en.getName()));
        }
    }

    public static File unzip(ZipFile file, ZipEntry entry) {
        File unz = new File(entry.getName());
        return unzip(file, entry, unz);
    }

    public static File unzip(ZipFile file, ZipEntry entry, File unzipped) {
        new File(unzipped.getParent()).mkdirs();

        ZipInputStream zis = null;
        try {
            zis = new ZipInputStream(file.getInputStream(entry));
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte buffer[] = new byte[1024];
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(unzipped);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert os != null;
        assert zis != null;

        int len;
        try {
            while ((len = zis.read(buffer)) > 0) {
                os.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return unzipped;
    }
}
