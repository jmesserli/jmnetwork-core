/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.file;

import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.logging.LogLevel;

import java.io.*;

public class FileUtils {

    public static void copyFolder(File src, File dest) {

        if (src.isDirectory()) {

            if (!dest.exists()) {
                dest.mkdir();
            }

            String files[] = src.list();

            for (String file : files) {
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                copyFolder(srcFile, destFile);
            }
        } else {
            InputStream in;
            OutputStream out;
            try {
                if (!dest.exists()) {
                    new File(dest.getParent()).mkdirs();
                    dest.createNewFile();
                }

                in = new FileInputStream(src);
                out = new FileOutputStream(dest);
            } catch (IOException e) {
                JMNetworkCore.logger.log(LogLevel.ERROR, String.format("[COPY FOLDER] File %s or %s not found", src.getPath(), dest.getPath()));
                return;
            }

            byte[] buffer = new byte[1024];

            int length;
            try {
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
            } catch (IOException e) {
                JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e, "while copying files");
            }

            try {
                in.close();
                out.close();
            } catch (IOException e) {
                JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e, "while closing streams");
            }
        }
    }

    public static void deleteDir(File dir) {
        if (!dir.exists()) return;
        assert dir != null;
        for (File f : dir.listFiles()) {
            if (f.isDirectory()) deleteDir(f);
            else {
                if (!f.delete()) JMNetworkCore.logger.log(LogLevel.ERROR, "Cannot delete file " + f.getAbsolutePath());
            }
        }
        if (!dir.delete()) JMNetworkCore.logger.log(LogLevel.ERROR, "Cannot delete folder " + dir.getAbsolutePath());
    }

    public static void deleteOnExit(File dir) {
        if (!dir.exists()) return;
        assert dir != null;
        for (File f : dir.listFiles()) {
            if (f.isDirectory()) deleteDir(f);
            else {
                f.deleteOnExit();
            }
        }
        dir.deleteOnExit();
    }
}
