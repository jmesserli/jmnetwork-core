/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.iterate;

abstract public class SimpleCountIterator {

    public int start, end;
    public int _cnt;


    public SimpleCountIterator() {}

    public SimpleCountIterator(int start, int end) {
        setStart(start).setEnd(end);
    }

    public SimpleCountIterator setStart(int start) {
        this.start = start;

        return this;
    }

    public SimpleCountIterator setEnd(int end) {
        this.end = end;

        return this;
    }

    public void iterate() {
        if (start == 0 && end == 0) return;

        if (start > end) {
            for (_cnt = start; _cnt >= end; _cnt--) {
                doIteration();
            }
        } else if (start < end) {
            for (_cnt = start; _cnt <= end; _cnt++) {
                doIteration();
            }
        } else {
            doIteration();
        }
    }

    abstract public void doIteration();
}
