/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.iterate;

abstract public class Runner {

    public int times;

    public Runner(int times) {
        if (times < 0) {
            throw new IllegalArgumentException("times must be greather than 0! (times=" + times + ")");
        }
        this.times = times;
    }

    abstract protected void doRun();

    public void run() {
        for (int i = 0; i < times; i++) {
            doRun();
        }
    }
}
