/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.number;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * User: joel / Date: 01.01.14 / Time: 00:46
 */
public class NumberUtils {

    public static ArrayList<Long> getParsedLongsFromString(String numbers, String separator) {
        ArrayList<Long> r_list = new ArrayList<>();

        for (String s : numbers.split(separator)) {
            r_list.add(Long.parseLong(s));
        }

        return r_list;
    }

    public static ArrayList<Integer> getParsedIntsFromString(String numbers, String separator) {
        ArrayList<Integer> r_list = new ArrayList<>();

        for (String s : numbers.split(separator)) {
            r_list.add(Integer.parseInt(s));
        }

        return r_list;
    }

    public static String addAppostrophesToNumber(long number) {
        return addAppostrophesToNumber(new BigInteger(number + ""));
    }

    public static String addAppostrophesToNumber(BigInteger number) {
        String num_string = number.toString();
        char[] num_string_charray = num_string.toCharArray();
        int numLength = num_string.length();
        int pos = 0;

        StringBuilder sb = new StringBuilder();

        for (int _cnt = numLength - 1; _cnt >= 0; _cnt--) {
            if (pos != 0 && pos % 3 == 0) sb.insert(0, "'");
            sb.insert(0, num_string_charray[_cnt]);
            pos++;
        }

        return sb.toString();
    }
}
