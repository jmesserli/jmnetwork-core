/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.yUML;

import ch.jmnetwork.core.tools.array.ArrayUtils;
import ch.jmnetwork.core.tools.code.Function;
import ch.jmnetwork.core.tools.reflection.ReflectionUtils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

public class YumlUtils {

    private static int cuttingLength = 30;

    private static Function<Field, String> fieldToString = toMap -> ReflectionUtils.parseModifiers(toMap.getModifiers()) + " " + cutMiddle(toMap.getName(), cuttingLength, "...") + ": " + toMap.getType().getSimpleName();
    private static Function<Class, String> classToSimpleString = Class::getSimpleName;
    private static Function<Method, String> methodToString = toMap -> {
        Class[] parameters = toMap.getParameterTypes();
        return ReflectionUtils.parseModifiers(toMap.getModifiers()) + " " + cutMiddle(toMap.getName(), cuttingLength, "...") + "(" + ArrayUtils.join(", ", classToSimpleString, parameters) + "): " + toMap.getReturnType().getSimpleName();
    };

    public static String classToYuml(Class c) {
        StringBuilder builder = new StringBuilder();
        String name = c.getSimpleName();

        Field[] fields = c.getDeclaredFields();
        Method[] methods = c.getDeclaredMethods();

        builder.append("[").append(name).append("|");
        builder.append(doReplacements(ArrayUtils.join(";", fieldToString, fields))).append("|");
        builder.append(doReplacements(ArrayUtils.join(";", methodToString, methods))).append("]");

        return builder.toString();
    }

    public static URI getURI(String uml) {
        String url = null;
        try {
            url = "http://yuml.me/diagram/plain;scale:180;/class/" + URLEncoder.encode(uml, "UTF-8").replaceAll("\\+","%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            return new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String doReplacements(String s) {
        return s.replaceAll("\\[", "\uff3b").replaceAll("\\]", "\uff3d").replaceAll("\\(", "\u2768").replaceAll("\\)", "\u2769").replaceAll(",", "\uff0c");
    }

    private static String cutMiddle(String toCut, int maxLength, String middle) {
        if (middle.length() >= maxLength) return null;
        if (toCut.length() <= maxLength) return toCut;
        int maxlength = maxLength - middle.length();
        int charactersTooMuch = toCut.length() - maxlength;
        int middlePos = toCut.length() / 2;

        String beginSubstr = toCut.substring(0, middlePos - (charactersTooMuch >> 1));
        String endSubstr = toCut.substring(middlePos + (charactersTooMuch >> 1));

        return beginSubstr + middle + endSubstr;
    }
}
