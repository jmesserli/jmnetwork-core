/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.time;

public class TimeMeasure {

    private long start, end;
    private boolean isRunning = false, ran = false;

    public void start() {
        start = System.nanoTime();
        isRunning = true;
        ran = false;
    }

    public void stop() {
        end = System.nanoTime();
        if (!isRunning) throw new IllegalStateException("This TimeMeasure was never started");

        isRunning = false;
        ran = true;
    }

    public long getNanosecondsNow() {
        if (!isRunning) throw new IllegalStateException("TimeMeasure is not running!");
        long now = System.nanoTime();
        return now - start;
    }

    public double getMilisecondsNow() {
        return (getNanosecondsNow() / 1000000D);
    }

    public float getSecondsNow() {
        return (float) (getMilisecondsNow() / 1000F);
    }

    public long getMillisecondsLong() {
        return (long) getMilliseconds();
    }

    public double getMilliseconds() {
        return (getNanoseconds() / 1000000D);
    }

    public long getNanoseconds() {
        if (!ran) throw new IllegalStateException("TimeMeasure never started / stopped.");
        return end - start;
    }

    public int getSecondsInt() {
        return Math.round(getSeconds());
    }

    public float getSeconds() {
        double d = getMilliseconds();
        return (float) (d / 1000F);
    }
}
