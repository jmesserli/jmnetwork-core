/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.time;

import ch.jmnetwork.core.tools.math.MathUtils;

import java.util.Observable;

public class Timer extends Observable {

    public final float tps;
    private int ticks = 0;
    private boolean running = false;
    private Thread myThread = null;
    private TimeMeasure notifyMeasure = new TimeMeasure(), tpsMeasure = new TimeMeasure();
    private boolean debug = false;


    public Timer(float ticks_per_second, boolean debug) {
        this(ticks_per_second);
        this.debug = debug;
    }

    public Timer(float ticks_per_second) {
        tps = ticks_per_second;
    }

    public void start() {
        if (running) return;

        myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                tpsMeasure.start();
                while (true) {
                    if (Thread.interrupted()) break;

                    setChanged();
                    notifyMeasure.start();
                    notifyObservers();
                    notifyMeasure.stop();

                    ticks++;

                    if (debug) System.out.print("nO: " + MathUtils.round(notifyMeasure.getMilliseconds(), 2) + "ms | ");

                    try {
                        Thread.sleep((long) (1000F / tps));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (tpsMeasure.getMilisecondsNow() >= 1000D && debug) {
                        tpsMeasure.stop();
                        System.out.println("\nReal TPS: " + ticks + " should-be-TPS: " + tps + " (+" + (ticks - tps) + " / *" + (ticks / tps) + " / " + MathUtils.round((ticks / tps) * 100, 1) + "%)");

                        ticks = 0;
                        tpsMeasure.start();
                    }
                }
            }
        });
        myThread.start();

        running = true;
    }

    public void stop() {
        if (running && myThread != null) {
            myThread.interrupt();
        }

        myThread = null;
        running = false;
    }
}
