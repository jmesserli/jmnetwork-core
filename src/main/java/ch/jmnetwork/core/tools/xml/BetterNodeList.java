/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.xml;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * An implementation of {@link org.w3c.dom.NodeList} which can be used not like NodeList itself...
 */
public class BetterNodeList implements NodeList, Iterable<Node> {

    private ArrayList<Node> nodelist = new ArrayList<>();

    public BetterNodeList() {}

    public BetterNodeList(NodeList nl) {
        for (int _cnt = 0; _cnt < nl.getLength(); _cnt++)
            add(nl.item(_cnt));
    }

    public void add(Node n) {
        nodelist.add(n);
    }

    @Override
    public Node item(int index) {
        return nodelist.get(index);
    }

    @Override
    public int getLength() {
        return nodelist.size();
    }

    @Override
    public Iterator<Node> iterator() {
        return nodelist.iterator();
    }

    public ArrayList<Node> getNodelist() {
        return nodelist;
    }
}
