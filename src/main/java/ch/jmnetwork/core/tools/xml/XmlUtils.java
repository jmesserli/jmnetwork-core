/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * User: joel / Date: 05.01.14 / Time: 14:44
 */
public class XmlUtils {

    /**
     * @param file The XML file
     * @return null if document could not be loaded
     */
    public static Document getXMLDocumentFromFile(File file) {
        File f = file;
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        Document document = null;
        try {
            builder = builderFactory.newDocumentBuilder();
            document = builder.parse(f);
            document.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return document;
    }

    public static Document getEmptyDocument(String rootelementName) {
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            return null;
        }
        Document d = builder.newDocument();
        Element root = d.createElement(rootelementName);
        d.appendChild(root);

        return d;
    }

    public static Node getFirstChildElementWithName(String elementName, Node node) {
        BetterNodeList nodeList = getAllChildren(node);

        for (Node c_node : nodeList) {
            if (c_node.getNodeName().equals(elementName)) {
                return c_node;
            }
        }

        return null;
    }

    public static BetterNodeList getAllChildren(Node node) {
        return new BetterNodeList(node.getChildNodes());
    }

    public static BetterNodeList getAllChildrenWithName(String name, Node node) {
        BetterNodeList nl = getAllChildren(node);
        ArrayList<Node> nodes = new ArrayList<>();

        for (Node n : nl) {
            if (n.getNodeName().equals(name)) nodes.add(n);
        }

        return listToNodeList(nodes);
    }

    public static String getChildText(String child, Node node) {
        Node n = getFirstChildElementWithName(child, node);

        return n.getTextContent();
    }

    public static ArrayList<String> getAllChildTexts(String child, Node node) {
        ArrayList<String> r_list = new ArrayList<>();
        BetterNodeList children = getAllChildrenWithName(child, node);

        for (Node n : children) {
            r_list.add(n.getTextContent());
        }

        return r_list;
    }

    public static BetterNodeList listToNodeList(ArrayList<Node> nodeArrayList) {
        BetterNodeList r_list = new BetterNodeList();
        for (Node n : nodeArrayList) {
            r_list.add(n);
        }

        return r_list;
    }

    public static Element getRootElement(Document document) {
        return document.getDocumentElement();
    }

    public static BetterNodeList getAllElementsWithName(String name, Element rootElement) {
        return new BetterNodeList(rootElement.getElementsByTagName(name));
    }

    public static String getAttributeValue(String attributeName, Node n) {
        NamedNodeMap attr = n.getAttributes();

        return attr.getNamedItem(attributeName).getTextContent();
    }

    public static ArrayList<String> getAllAttributeValues(String nodeName, String attributeName, Node n) {
        ArrayList<String> r_list = new ArrayList<>();
        BetterNodeList nodes = getAllChildrenWithName(nodeName, n);

        for (Node node : nodes) {
            NamedNodeMap attr = node.getAttributes();

            r_list.add(attr.getNamedItem(attributeName).getTextContent());
        }

        return r_list;
    }
}
