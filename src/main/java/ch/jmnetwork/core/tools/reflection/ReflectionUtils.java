/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.reflection;

import ch.jmnetwork.core.tools.array.ArrayUtils;
import ch.jmnetwork.core.tools.code.Function;
import ch.jmnetwork.core.tools.list.ListUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

/**
 * User: joel / Date: 30.12.13 / Time: 17:29
 */
public class ReflectionUtils {

    /**
     * Gets a the value of a specific attribute of an annotation
     *
     * @param theClass      The class where the annotation is attached
     * @param theAnnotation The annotation to get the value from
     * @param attribute     The attribute to get
     * @return value of {@code attribute} in {@code theAnnotation} on Class {@code theClass}
     */
    public static String getClassAnnotationValue(Class theClass, Class theAnnotation, String attribute) {
        String value = null;

        Annotation annotation = theClass.getAnnotation(theAnnotation);
        if (annotation != null) {
            try {
                value = (String) annotation.annotationType().getMethod(attribute).invoke(annotation);
            } catch (Exception ex) {
            }
        }

        return value;
    }

    public static ArrayList<String[]> getConstructorsAsStringListWithDescription(Constructor[] constructors) {
        return ListUtils.addItemToTopOfList(getConstructorsAsStringList(constructors), new String[]{"Modifiers", "Name", "Arguments"});
    }

    public static ArrayList<String[]> getConstructorsAsStringList(Constructor[] constructors) {
        ArrayList<String> t_constructor_modifiers = new ArrayList<>(), t_constructor_names = new ArrayList<>(), t_constructor_arguments = new ArrayList<>();

        for (Constructor constructor : constructors) {
            t_constructor_names.add(constructor.getName());
            t_constructor_modifiers.add(parseModifiers(constructor.getModifiers()));
            t_constructor_arguments.add(classArrayToSimpleString(constructor.getParameterTypes()));
        }

        return ListUtils.getListWithArraysOfLists(String.class, t_constructor_modifiers, t_constructor_names, t_constructor_arguments);
    }

    public static ArrayList<String[]> getFieldsAsStringListWithDescription(Field[] fields) {
        return ListUtils.addItemToTopOfList(ReflectionUtils.getFieldsAsStringList(fields), new String[]{"Modifiers", "Field Name", "Type"});
    }

    public static ArrayList<String[]> getFieldsAsStringList(Field[] fields) {
        ArrayList<String> t_field_names = new ArrayList<>(), t_field_types = new ArrayList<>(), t_field_modifiers = new ArrayList<>();

        for (int _cnt = 0; _cnt < fields.length; _cnt++) {
            Field c_field = fields[_cnt];
            t_field_names.add(c_field.getName());
            t_field_types.add(c_field.getType().getSimpleName());
            t_field_modifiers.add(getModifierStringFrom(c_field));
        }

        return ListUtils.getListWithArraysOfLists(String.class, t_field_modifiers, t_field_names, t_field_types);
    }

    public static ArrayList<String[]> getMethodsAsStringListWithDescription(Method[] methods) {
        return ListUtils.addItemToTopOfList(getMethodsAsStringList(methods), new String[]{"Modifiers", "Name", "Arguments", "Return Type"});
    }

    public static ArrayList<String[]> getMethodsAsStringList(Method[] methods) {
        ArrayList<String> t_method_modifiers = new ArrayList<>(), t_method_names = new ArrayList<>(), t_method_arguments = new ArrayList<>(), t_method_return_types = new ArrayList<>();

        for (int _cnt = 0; _cnt < methods.length; _cnt++) {
            Method c_method = methods[_cnt];

            t_method_arguments.add(classArrayToSimpleString(c_method.getParameterTypes()));
            t_method_modifiers.add(parseModifiers(c_method.getModifiers()));
            t_method_names.add(c_method.getName());
            t_method_return_types.add(c_method.getReturnType().getSimpleName());
        }

        return ListUtils.getListWithArraysOfLists(String.class, t_method_modifiers, t_method_names, t_method_arguments, t_method_return_types);
    }

    public static String classArrayToSimpleString(Class[] classes) {
        if (classes.length == 0) return "null";

        return ArrayUtils.join(", ", new Function<Class, String>() {
            @Override
            public String map(Class toMap) {
                return toMap.getSimpleName();
            }
        }, classes);
    }

    public static Constructor[] getClassConstructors(Class c) {
        return c.getConstructors();
    }

    public static Field[] getFieldsFromClass(Class c) {
        return c.getDeclaredFields();
    }

    public static Method[] getMethodsFromClass(Class c) {
        return c.getDeclaredMethods();
    }

    public static String getModifierStringFrom(Constructor constructor) {
        return parseModifiers(constructor.getModifiers());
    }

    public static String getModifierStringFrom(Class c) {
        return parseModifiers(c.getModifiers());
    }

    public static String getModifierStringFrom(Field f) {
        return parseModifiers(f.getModifiers());
    }

    public static String parseModifiers(int mods) {
        String ret_str = "";

        //VISIBILITY
        ret_str += Modifier.isPublic(mods) ? "public" : Modifier.isProtected(mods) ? "protected" : Modifier.isPrivate(mods) ? "private" : "default";

        //OTHERS
        ret_str += Modifier.isAbstract(mods) ? " abstract" : "";
        ret_str += Modifier.isStatic(mods) ? " static" : "";
        ret_str += Modifier.isFinal(mods) ? " final" : "";
        ret_str += Modifier.isTransient(mods) ? " transient" : "";
        ret_str += Modifier.isVolatile(mods) ? " volatile" : "";
        ret_str += Modifier.isSynchronized(mods) ? " synchronized" : "";
        ret_str += Modifier.isNative(mods) ? " native" : "";
        ret_str += Modifier.isStrict(mods) ? " strictfp" : "";

        return ret_str;
    }
}