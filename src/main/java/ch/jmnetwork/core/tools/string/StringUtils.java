/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.string;

import ch.jmnetwork.core.tools.list.ListUtils;

import java.util.ArrayList;

import static ch.jmnetwork.core.tools.list.ListUtils.getListFromIndexInArraysInList;

/**
 * User: joel / Date: 30.12.13 / Time: 16:38
 */
public class StringUtils {


    @SafeVarargs
    public static String getColumnizedString(ArrayList<String>... columns) {
        return getColumnizedString(ListUtils.getListWithArraysOfLists(String.class, columns));
    }

    public static String getColumnizedString(ArrayList<String[]> list) {
        if (list.size() == 0) return null;
        String r_string = "";
        int col_count = list.get(0).length;
        ArrayList<Integer> longestList = new ArrayList<>();

        for (int _cnt = 0; _cnt < col_count; _cnt++)
            longestList.add(longestStringLength(getListFromIndexInArraysInList(list, _cnt)));

        for (String[] array : list) {
            r_string += String.format(getFormattingString(col_count, longestList.toArray(new Integer[longestList.size()])), array);
        }

        return r_string;
    }

    public static int longestStringLength(ArrayList<String> strings) {
        int tmp = 0;

        for (String t_string : strings)
            if (t_string != null) tmp = t_string.length() > tmp ? t_string.length() : tmp;

        return tmp;
    }

    private static String getFormattingString(int columns, Integer... lengths) {
        String t_str = "";

        for (int _cnt = 0; _cnt < columns; _cnt++) {
            t_str += String.format("%%%d%s", lengths[_cnt], _cnt + 1 == columns ? "s" : "s  ");
        }

        t_str += "%n";

        return t_str;
    }

    public static String listToOneLineString(ArrayList list) {
        String r_str = "";

        for (int _cnt = 0; _cnt < list.size(); _cnt++) {
            String temp = list.get(_cnt).toString();
            r_str += _cnt + 1 == list.size() ? temp : temp + ", ";
        }

        return r_str;
    }
}
