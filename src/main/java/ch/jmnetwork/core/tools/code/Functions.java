/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.code;

import java.util.ArrayList;

public class Functions {

    public static Function<ArrayList<Integer>, ArrayList<Long>> INT_ARRAYLIST_TO_LONG_ARRAYLIST = toMap -> {
        ArrayList<Long> returnList = new ArrayList<>();

        for (Integer i : toMap) {
            returnList.add(i.longValue());
        }

        return returnList;
    };
    public static Function<Object, String> OBJECT_TO_STRING = Object::toString;

    public static <T, R> R convert(T from, Function<T, R> function) {
        return function.map(from);
    }
}