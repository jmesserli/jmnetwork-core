/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.code;

/**
 * An alternative, <i>generic</i> implementation of an Observer system
 *
 * @param <T> The type this observer accepts
 */
public interface TypeObserver<T> {
    /**
     * Gets called by an {@link ch.jmnetwork.core.tools.code.TypeObservable} when it updates
     *
     * @param o   The {@link ch.jmnetwork.core.tools.code.TypeObservable} itself
     * @param arg The argument of type <b>T</b>, can be null
     */
    void update(TypeObservable<T> o, T arg);
}
