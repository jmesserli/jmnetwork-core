/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.code;

/**
 * A simple datatype which can hold one object of two different datatypes <i>(Generic)</i>
 *
 * @param <X> type of the first object
 * @param <Y> type of the second object
 */
public class MultitypeBag<X, Y> {

    private X element1;
    private Y element2;

    /**
     * Constructor
     *
     * @param e1 The first element to be stored
     * @param e2 The second element to be stored
     */
    public MultitypeBag(X e1, Y e2) {
        element1 = e1;
        element2 = e2;
    }

    /**
     * Gets the first element
     *
     * @return the first element
     */
    public X get1() {
        return element1;
    }

    /**
     * Sets the first element
     *
     * @param e1 element to set
     */
    public void set1(X e1) {
        element1 = e1;
    }

    /**
     * Gets the second element
     *
     * @return the second element
     */
    public Y get2() {
        return element2;
    }

    /**
     * Sets the second element
     *
     * @param e2 the element to set
     */
    public void set2(Y e2) {
        element2 = e2;
    }
}
