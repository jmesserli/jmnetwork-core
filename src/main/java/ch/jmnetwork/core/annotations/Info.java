/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.annotations;

import java.lang.annotation.*;

/**
 * Gives Information about a class / module / project
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Info {
    /**
     * The name of the class / module / project
     */
    public String name();

    /**
     * The internal name of the class / module / project
     */
    public String internalName() default "";

    /**
     * The version of the class / module / project
     */
    public String version() default "1.0";

    /**
     * The author(s) of the class / module / project
     */
    public String[] authors() default "Multiple Authors";
}
