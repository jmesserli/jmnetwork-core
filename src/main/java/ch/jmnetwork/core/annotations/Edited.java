/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks something as edited in the sourcecode, should give the ability to leave a short note what was changed when.
 */
@Retention(RetentionPolicy.SOURCE)
public @interface Edited {
    /**
     * The person(s) who edited it
     */
    public String[] editedBy() default "";

    /**
     * The <i>simplified</i> date and time of the last edit (e.g. 18.01.2014 10: for 18.01.2014 10:35)
     */
    public String lastEdited();

    /**
     * What was changed
     */
    public String[] changes() default "code changes";
}
