/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.logging;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date: 30.11.13
 * Time: 10:07
 *
 * @author joel
 */
public class Logger {

    private File logFile;
    private FileWriter fw;
    private PrintStream logPrintStream = null;
    protected PrintStream defaultOut = System.out;

    LogLevel displayedLogLevel;

    final String programName;

    /**
     * See the extended constructor {@link #Logger(java.io.File, String, boolean)} for more information.
     *
     * @see #Logger(java.io.File, String, boolean)
     */
    public Logger(File file, String programName) {
        this(file, programName, false);
    }

    /**
     * Constructor (Extended)
     *
     * @param file        Output goes into this file
     * @param programName The name of the program, used to prefix
     * @param append      Append to the logfile or start with an empty file everytime?
     * @since 0.5 alpha
     */
    public Logger(File file, String programName, boolean append) {
        setDisplayedLogLevel(LogLevel.ALL);
        this.programName = programName;
        logFile = file;

        try {
            fw = new FileWriter(logFile, append); // true => append to file
        } catch (IOException e) {
            // File doesn't exist?
            if (!logFile.exists()) {
                logFile.mkdirs();
                try {
                    logFile.createNewFile();
                } catch (IOException e1) {
                }
            } else {
                log(LogLevel.ERROR, "Error while opening file " + logFile.getAbsolutePath());
            }
        }
    }

    public void setDisplayedLogLevel(LogLevel level) {
        displayedLogLevel = level;
    }

    public void log(LogLevel logLevel, String message) {
        if (!logLevel.isMoreSpecificThan(this.displayedLogLevel)) {
            (!logLevel.isMoreSpecificThan(LogLevel.ERROR) ? System.err : System.out).println(getFinalMessage(logLevel, message));
        }

        try {
            fw.write(getFinalMessage(logLevel, message) + "\n");
            fw.flush();
        } catch (IOException e) {
        }
    }

    public void log(LogLevel logLevel, Object message) {
        log(logLevel, message.toString());
    }

    public void fail(LogLevel logLevel, String failMessage) {
        log(logLevel, failMessage);
        System.exit(-1);
    }

    public void logFileOnly(LogLevel level, String message) {
        try {
            fw.write(getFinalMessage(level, message) + "\n");
            fw.flush();
        } catch (IOException e) {
        }
    }

    public void logStackTrace(LogLevel level, Throwable throwable) {
        logStackTrace(level, throwable, "");
    }

    public void logStackTrace(LogLevel level, Throwable throwable, String comment) {
        if (!comment.equals("")) log(level, comment);

        Throwable cause = throwable.getCause();


        log(level, cause != null ? cause.toString() : throwable.getMessage());
        for (StackTraceElement e : throwable.getStackTrace()) {
            log(level, e.toString());
        }
    }

    @Deprecated
    public void logStackTrace(LogLevel level, StackTraceElement[] elements, String comment) {
        log(level, comment);
        logStackTrace(level, elements);
    }

    @Deprecated
    public void logStackTrace(LogLevel level, StackTraceElement[] elements) {
        for (StackTraceElement e : elements) {
            log(level, e.toString());
        }
    }

    public PrintStream getLogStream(LogLevel logLevel) {
        LogOutputStream stream = new LogOutputStream(this, logLevel);
        logPrintStream = new PrintStream(stream);

        return logPrintStream;
    }

    private String getFinalMessage(LogLevel logLevel, String message) {
        String timeString = SimpleDateFormat.getDateTimeInstance().format(new Date());
        String logLString = String.format("[%s]", logLevel.text);
        String programNameString = String.format("[%s]", programName);

        return String.format("%s | %s %s: %s", timeString, programNameString, logLString, message);
    }
}