/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.logging;

/**
 * User: joel / Date: 30.11.13 / Time: 10:08
 */
public enum LogLevel {

    ALL(7, "ALL"),
    TRACE(6, "TRACE"),
    DEBUG(5, "DEBUG"),
    INFO(4, "INFO"),
    WARNING(3, "WARNING"),
    ERROR(2, "ERROR"),
    FATAL(1, "FATAL"),
    OFF(0, "OFF");

    int specificIndex;
    String text;

    private LogLevel(int specificIndex, String text) {
        this.specificIndex = specificIndex;
        this.text = text;
    }

    public boolean isMoreSpecificThan(LogLevel level) {
        return this.specificIndex > level.specificIndex;
    }

    public boolean isMoreSpecificOrEqualTo(LogLevel level) {
        return this.specificIndex >= level.specificIndex;
    }

}
