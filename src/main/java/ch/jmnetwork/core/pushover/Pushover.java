/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.pushover;

import ch.jmnetwork.core.tools.network.RequestUtils;

import java.util.HashMap;

public class Pushover {

    String api_key;
    String client_id;

    String baseurl = "https://api.pushover.net/1/messages.json";

    public Pushover(String api_key, String client_id) {
        this.api_key = api_key;
        this.client_id = client_id;
    }

    public void sendNotification(String message) {
        sendNotification("", message, "", "", -1);
    }

    public void sendNotification(String title, String message) {
        sendNotification(title, message, "", "", -1);
    }

    public void sendNotification(String title, String message, String url) {
        sendNotification(title, message, url, "", -1);
    }

    public void sendNotification(String title, String message, String url, String url_title) {
        sendNotification(title, message, url, url_title, -1);
    }

    public void sendNotificationHighPriority(String message) {
        sendNotification("", message, "", "", 1);
    }

    public void sendNotificationHighPriority(String title, String message) {
        sendNotification(title, message, "", "", 1);
    }

    public void sendNotificationHighPriority(String title, String message, String url) {
        sendNotification(title, message, url, "", 1);
    }

    public void sendNotificationHighPriority(String title, String message, String url, String url_title) {
        sendNotification(title, message, url, url_title, 1);
    }

    public void sendNotificationConfirm(String message) {
        sendNotification("", message, "", "", 2);
    }

    public void sendNotificationConfirm(String title, String message) {
        sendNotification(title, message, "", "", 2);
    }

    public void sendNotificationConfirm(String title, String message, String url) {
        sendNotification(title, message, url, "", 2);
    }

    public void sendNotificationConfirm(String title, String message, String url, String url_title) {
        sendNotification(title, message, url, url_title, 2);
    }

    public void sendNotification(String title, String message, String url, String url_title, int priority, String... extra) {
        HashMap<String, String> arguments = new HashMap<>();
        arguments.put("token", api_key);
        arguments.put("user", client_id);
        if (title.length() != 0) arguments.put("title", title);
        arguments.put("message", message);
        if (url.length() != 0) {
            arguments.put("url", url);
            if (url_title.length() != 0) arguments.put("url_title", url_title);
        }
        if (priority != -1) {
            arguments.put("priority", "" + priority);
            if (priority == 2) {
                arguments.put("expire", "" + 86400);
                arguments.put("retry", "" + 60);
            }
        }
        if (extra.length > 0) {
            if (extra.length % 2 == 0) {
                int _cnt = 0;
                for (int i = 0; i < (extra.length / 2); i++) {
                    arguments.put(extra[_cnt], extra[_cnt + 1]);
                    _cnt += 2;
                }
            } else {
                throw new IllegalArgumentException("extra.length % 2 != 0");
            }
        }

        RequestUtils.doPost(baseurl, arguments);
    }
}
