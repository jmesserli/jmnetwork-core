/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.localization;

import ch.jmnetwork.core.CoreReference;
import ch.jmnetwork.core.storage.StorageHandler;

import java.io.*;
import java.util.HashMap;
import java.util.Properties;

/**
 * User: joel / Date: 06.01.14 / Time: 16:55
 */
public class Localization implements Serializable {

    HashMap<String, String> mappings = new HashMap<>();

    public Localization() {
    }

    public static Localization loadLocalization(File flat) throws IOException {
        Localization loc = new Localization();

        Properties p = new Properties();
        p.load(new FileInputStream(flat));

        for (Object o : p.keySet()) {
            String key = (String) o;
            String val = p.getProperty(key);

            loc.addMapping(key, val);
        }

        return loc;
    }

    public static Localization loadLocalization(File storage, String propertyName) {
        return (Localization) new StorageHandler(storage).getStorageObjectByName(propertyName).getObject();
    }

    public void saveLocalization(File file, String propertyName) {
        StorageHandler storageHandler = new StorageHandler(file);

        storageHandler.setProperty(propertyName, this, Localization.class);
        storageHandler.saveStorage();
    }

    public void saveLocalization(File file) throws IOException {
        Properties p = new Properties();

        for (String s : mappings.keySet()) {
            p.put(s, mappings.get(s));
        }

        p.store(new FileWriter(file), "JMNetworkLocalization " + CoreReference.VERSION);
    }


    public void addMapping(String unlocalizedName, String localizedName) {
        mappings.put(unlocalizedName, localizedName);
    }

    public String getLocalizedString(String unlocalizedName) {
        if (!mappings.containsKey(unlocalizedName)) return null;
        return mappings.get(unlocalizedName);
    }
}
