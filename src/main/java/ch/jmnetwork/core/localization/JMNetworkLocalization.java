/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.localization;

import ch.jmnetwork.core.storage.StorageObject;

/**
 * User: joel / Date: 06.01.14 / Time: 17:25
 */
public class JMNetworkLocalization {
    private static String localization_string = "rO0ABXNyACtjaC5qbW5ldHdvcmsuY29yZS5sb2NhbGl6YXRpb24uTG9jYWxpemF0aW9u+9GATHbFpDgCAAFMAAhtYXBwaW5nc3QAE0xqYXZhL3V0aWwvSGFzaE1hcDt4cHNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAx3CAAAABAAAAAFdAAhc3RvcmFnZS5lbGVtZW50cy5yb290ZWxlbWVudC5uYW1ldAAHc3RvcmFnZXQAHXN0b3JhZ2UuYXR0cmlidXRlcy5jbGFzcy5uYW1ldAAFY2xhc3N0ACNzdG9yYWdlLmVsZW1lbnRzLnN0b3JhZ2VvYmplY3QubmFtZXQADXN0b3JhZ2VvYmplY3R0ABxzdG9yYWdlLmF0dHJpYnV0ZXMubmFtZS5uYW1ldAAEbmFtZXQAG3N0b3JhZ2UuZWxlbWVudHMudmFsdWUubmFtZXQABXZhbHVleA==";
    private static Localization storage_localization = (Localization) new StorageObject<>("localization", localization_string, Localization.class).getObject();

    public static Localization getStorageLocalization() {
        return storage_localization;
    }

    /**
     * Sets the localization for the Storage Handler
     * <p>
     * You need to localize the following:
     * <ul>
     * <li>storage.attributes.name.name - name of the name attribute</li>
     * <li>storage.attributes.class.name - name of the class attribute</li>
     * <li>storage.elements.value.name - name of the value tag</li>
     * <li>storage.elements.rootelement.name - name of the xml root element</li>
     * <li>storage.elements.storageobject.name - name of the storageobject tag</li>
     * </ul>
     *
     * @param storageLocalization {@link ch.jmnetwork.core.localization.Localization}
     */
    public static void setStorageLocalization(Localization storageLocalization) {
        storage_localization = storageLocalization;
    }
}
