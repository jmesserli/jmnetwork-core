/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.server;

import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.logging.LogLevel;
import ch.jmnetwork.core.tools.time.Timer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

abstract public class Server implements Observer {

    private ServerSocket serverSocket;
    private volatile ArrayList<ServerClient> connectedClients = new ArrayList<>();

    public Server(int serverport) {
        try {
            serverSocket = new ServerSocket(serverport);
        } catch (IOException e) {
            throw new IllegalStateException("Cannot create serversocket, make sure the port is not already used");
        }
    }

    public void startAcceptingClients() {
        new Timer(0.5F).addObserver(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Socket newClient = null;
                    try {
                        newClient = serverSocket.accept();
                    } catch (IOException e) {
                        JMNetworkCore.logger.log(LogLevel.ERROR, "[Server:" + serverSocket.getLocalPort() + "]: Cannot accept client");
                    }

                    if (newClient == null) continue;

                    connectedClients.add(new ServerClient(newClient));
                    onClientConnect(connectedClients.get(connectedClients.size() - 1), connectedClients.size());
                }
            }
        }).start();
    }

    abstract public void onClientConnect(ServerClient serverClient, int numClientsConnected);

    abstract public void onClientDisconnect(ServerClient serverClient, int numClientsConnected);

    public void disconnectClient(ServerClient serverClient) {
        if (connectedClients.contains(serverClient)) {
            try {
                serverClient.socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            connectedClients.remove(serverClient);
            onClientDisconnect(serverClient, connectedClients.size());
        } else {
            throw new IllegalArgumentException("This client is not connected to this server!");
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        for (ServerClient c : connectedClients) {
            if (!c.socket.isConnected()) {
                connectedClients.remove(c);
                onClientDisconnect(c, connectedClients.size());
            }
        }
    }
}