/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui;

import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.gui.components.JButton;
import ch.jmnetwork.core.gui.components.JComponent;
import ch.jmnetwork.core.gui.event.JEvent;
import ch.jmnetwork.core.logging.LogLevel;
import ch.jmnetwork.core.tools.time.Timer;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * The JMNetwork GUI Manager
 * <p>
 * It handles all the drawing stuff, hands events to the {@link ch.jmnetwork.core.gui.components.JComponent}s
 */
public class JGUIManager implements Observer {

    private List<JComponent> componentList = new ArrayList<>(), mouseEventList = new ArrayList<>(), keyEventList = new ArrayList<>();
    private Paint background = Color.white;
    private JDrawingPane pane;
    private Dimension size;
    private Timer repaintTimer = null;

    /**
     * Constructor.
     *
     * @param size The size of the window
     */
    public JGUIManager(Dimension size) {
        this.size = size;
    }

    /**
     * Constructor.
     *
     * @param size_x The x size of the window
     * @param size_y The y size of the window
     */
    public JGUIManager(int size_x, int size_y) {
        this.size = new Dimension(size_x, size_y);
    }

    /**
     * Called by JDrawingPane when an event arrives.
     *
     * @param event The {@link ch.jmnetwork.core.gui.event.JEvent} which happened
     */
    public void onEvent(JEvent event) {
        switch (event.type) {
            case KEY_EVENT:
                for (JComponent component : keyEventList) component.onEvent(event);
                break;
            case MOUSE_EVENT:
                for (JComponent component : mouseEventList) component.onEvent(event);
        }
    }

    /**
     * Sets the {@link ch.jmnetwork.core.gui.JDrawingPane} which this GUI Manager should manage.
     * <p/> <b><i>This normally gets called by the {@link ch.jmnetwork.core.gui.JDrawingPane} itself.</i></b>
     *
     * @param pane The {@link ch.jmnetwork.core.gui.JDrawingPane} to manage
     */
    public void setDrawingPane(JDrawingPane pane) {
        this.pane = pane;
    }

    /**
     * Sets the background of the managed {@link ch.jmnetwork.core.gui.JDrawingPane}
     *
     * @param paint The paint to set
     */
    public void setBackground(Paint paint) {
        this.background = paint;
    }

    /**
     * Add a {@link ch.jmnetwork.core.gui.components.JComponent} to the {@link ch.jmnetwork.core.gui.JDrawingPane}
     *
     * @param component The {@link ch.jmnetwork.core.gui.components.JComponent} to add
     */
    public void addComponent(JComponent component) {
        component.init(this);
        componentList.add(component);

        if (component.takesEvent(JEvent.EventType.MOUSE_EVENT)) mouseEventList.add(component);
        if (component.takesEvent(JEvent.EventType.KEY_EVENT)) keyEventList.add(component);
    }

    /**
     * Handles the drawing of all the components.
     * <p>
     * <b>Normally gets called by the managed {@link ch.jmnetwork.core.gui.JDrawingPane}</b>
     *
     * @param graphics The graphics object which will be used by all the components to draw themselves
     */
    public void draw(Graphics graphics) {
        Graphics2D g2 = (Graphics2D) graphics;

        g2.setPaint(background);
        g2.fillRect(0, 0, size.width, size.height);
        for (JComponent component : componentList) component.draw(g2);
    }

    /**
     * Used by a {@link ch.jmnetwork.core.gui.components.JComponent} if a change happened and the window needs to be repainted.
     *
     * @param requester
     */
    public void requestRepaint(JComponent requester) {
        if (!componentList.contains(requester)) return;
        JMNetworkCore.logger.log(LogLevel.DEBUG, (requester instanceof JButton ? "JButton " + ((JButton) requester).getText() : requester) + ": Requested repaint");
        repaint();
    }

    private void repaint() {
        pane.repaint();
    }

    /**
     * Get the window size
     * <p>
     * <b>Used by {@link ch.jmnetwork.core.gui.JDrawingPane}'s {@link JDrawingPane#getPreferredSize()}</b>
     *
     * @return the windowsize set in {@link #JGUIManager(java.awt.Dimension)} or {@link #JGUIManager(int, int)}
     */
    public Dimension getSize() {
        return size;
    }

    /**
     * Sets a repaint timer which then repaints the Drawing Pane every so often
     *
     * @param timer The timer to use
     */
    public void setRepaintTimer(Timer timer) {
        if (repaintTimer != null) {
            repaintTimer.stop();
            repaintTimer.deleteObserver(this);
        }
        if (timer != null) {
            timer.addObserver(this);
            timer.start();
        }
        repaintTimer = timer;
    }

    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }
}
