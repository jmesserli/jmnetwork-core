/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.components;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class JClock extends JComponent {
    private Point center;
    private boolean showSeconds, exactSeconds = true;
    private float hourStart = -0.05F, hourEnd = 0.4F, minuteStart = -0.05F, minuteEnd = 0.8F, secondStart = -0.05F, secondEnd = 0.85F, hourLineStart = 0.8F, hourLineEnd = 0.95F, minuteLineStart = 0.85F, minuteLineEnd = 0.95F;
    private int hourThickness = 3, minuteThickness = 2, secondThickness = 1, outerThickness = 1, hourLineThickness = 2, minuteLineThickness = 1;
    private Paint minutePaint = Color.black, hourPaint = Color.black, secondPaint = Color.red, textPaint = Color.blue, hourLinesPaint = Color.black, minuteLinesPaint = Color.darkGray;

    /**
     * Constructor
     *
     * @param x    the x coordinate of the Component
     * @param y    the y coordinate of the Component
     * @param size the width and height of the Component
     */
    public JClock(int x, int y, int size, boolean seconds) {
        super(x, y, size, size);
        center = new Point(x + (size / 2), y + (size / 2));
        showSeconds = seconds;
        backgroundPaint = Color.black;
    }

    public JClock(int x, int y, int size) {
        this(x, y, size, true);
    }

    @Override
    public void draw(Graphics2D graphics) {
        //Check parent
        if (parent != null) {
            center = new Point(getX() + (getWidth() / 2), getY() + (getWidth() / 2));
        }

        Calendar c = new GregorianCalendar();
        ArrayList<ClockLine> lines = new ArrayList<>();
        float minuteAngle, hourAngle, secondAngle;

        int minute = c.get(Calendar.MINUTE), hour = c.get(Calendar.HOUR), second = c.get(Calendar.SECOND);
        minuteAngle = (minute + (second / 60F)) * (360 / 60F);
        hourAngle = (hour + (minute / 60F) % 12F) * (360 / 12F); // %12 because we need to show only hours 0-12, but we have 24 hours
        secondAngle = (second + (exactSeconds ? c.get(Calendar.MILLISECOND) / 1000F : 0)) * (360 / 60F);

        for (int i = 0; i < 60; i++) {
            lines.add(new ClockLine(minuteLineStart, minuteLineEnd, (360 / 60) * (i + 1), minuteLineThickness, minuteLinesPaint));
        }

        for (int i = 0; i < 12; i++) {
            lines.add(new ClockLine(hourLineStart, hourLineEnd, (360 / 12) * (i + 1), hourLineThickness, hourLinesPaint));
        }

        lines.add(new ClockLine(minuteStart, minuteEnd, minuteAngle, minuteThickness, minutePaint));
        lines.add(new ClockLine(hourStart, hourEnd, hourAngle, hourThickness, hourPaint));
        lines.add(new ClockLine(secondStart, secondEnd, secondAngle, secondThickness, secondPaint));

        Shape myShape = new Ellipse2D.Double(getX(), getY(), getHeigth() - 1, getHeigth() - 1);

        // Draw all the stuff
        graphics.setPaint(backgroundPaint);
        graphics.fill(myShape);

        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        for (ClockLine line : lines) {
            graphics.setPaint(line.paint);
            graphics.setStroke(line.stroke);
            graphics.draw(line.getLine());
        }

        graphics.setPaint(foregroundPaint);
        graphics.setStroke(new BasicStroke(outerThickness));
        graphics.draw(myShape);
    }

    public boolean isShowSeconds() {
        return showSeconds;
    }

    public void setShowSeconds(boolean showSeconds) {
        this.showSeconds = showSeconds;
    }

    public boolean isExactSeconds() {
        return exactSeconds;
    }

    public void setExactSeconds(boolean exactSeconds) {
        this.exactSeconds = exactSeconds;
    }

    public float getHourStart() {
        return hourStart;
    }

    public void setHourStart(float hourStart) {
        this.hourStart = hourStart;
    }

    public float getHourEnd() {
        return hourEnd;
    }

    public void setHourEnd(float hourEnd) {
        this.hourEnd = hourEnd;
    }

    public float getMinuteStart() {
        return minuteStart;
    }

    public void setMinuteStart(float minuteStart) {
        this.minuteStart = minuteStart;
    }

    public float getMinuteEnd() {
        return minuteEnd;
    }

    public void setMinuteEnd(float minuteEnd) {
        this.minuteEnd = minuteEnd;
    }

    public float getSecondStart() {
        return secondStart;
    }

    public void setSecondStart(float secondStart) {
        this.secondStart = secondStart;
    }

    public float getSecondEnd() {
        return secondEnd;
    }

    public void setSecondEnd(float secondEnd) {
        this.secondEnd = secondEnd;
    }

    public float getHourLineStart() {
        return hourLineStart;
    }

    public void setHourLineStart(float hourLineStart) {
        this.hourLineStart = hourLineStart;
    }

    public float getHourLineEnd() {
        return hourLineEnd;
    }

    public void setHourLineEnd(float hourLineEnd) {
        this.hourLineEnd = hourLineEnd;
    }

    public float getMinuteLineStart() {
        return minuteLineStart;
    }

    public void setMinuteLineStart(float minuteLineStart) {
        this.minuteLineStart = minuteLineStart;
    }

    public float getMinuteLineEnd() {
        return minuteLineEnd;
    }

    public void setMinuteLineEnd(float minuteLineEnd) {
        this.minuteLineEnd = minuteLineEnd;
    }

    public int getHourThickness() {
        return hourThickness;
    }

    public void setHourThickness(int hourThickness) {
        this.hourThickness = hourThickness;
    }

    public int getMinuteThickness() {
        return minuteThickness;
    }

    public void setMinuteThickness(int minuteThickness) {
        this.minuteThickness = minuteThickness;
    }

    public int getSecondThickness() {
        return secondThickness;
    }

    public void setSecondThickness(int secondThickness) {
        this.secondThickness = secondThickness;
    }

    public int getOuterThickness() {
        return outerThickness;
    }

    public void setOuterThickness(int outerThickness) {
        this.outerThickness = outerThickness;
    }

    public int getHourLineThickness() {
        return hourLineThickness;
    }

    public void setHourLineThickness(int hourLineThickness) {
        this.hourLineThickness = hourLineThickness;
    }

    public int getMinuteLineThickness() {
        return minuteLineThickness;
    }

    public void setMinuteLineThickness(int minuteLineThickness) {
        this.minuteLineThickness = minuteLineThickness;
    }

    public Paint getMinutePaint() {
        return minutePaint;
    }

    public void setMinutePaint(Paint minutePaint) {
        this.minutePaint = getRelativePaintIfNeccessary(minutePaint);
    }

    public Paint getHourPaint() {
        return hourPaint;
    }

    public void setHourPaint(Paint hourPaint) {
        this.hourPaint = getRelativePaintIfNeccessary(hourPaint);
    }

    public Paint getSecondPaint() {
        return secondPaint;
    }

    public void setSecondPaint(Paint secondPaint) {
        this.secondPaint = getRelativePaintIfNeccessary(secondPaint);
    }

    public Paint getTextPaint() {
        return textPaint;
    }

    public void setTextPaint(Paint textPaint) {
        this.textPaint = getRelativePaintIfNeccessary(textPaint);
    }

    public Paint getHourLinesPaint() {
        return hourLinesPaint;
    }

    public void setHourLinesPaint(Paint hourLinesPaint) {
        this.hourLinesPaint = getRelativePaintIfNeccessary(hourLinesPaint);
    }

    public Paint getMinuteLinesPaint() {
        return minuteLinesPaint;
    }

    public void setMinuteLinesPaint(Paint minuteLinesPaint) {
        this.minuteLinesPaint = getRelativePaintIfNeccessary(minuteLinesPaint);
    }

    @Override
    public String toString() {
        return String.format("JClock@%s", Integer.toHexString(hashCode()));
    }

    private class ClockLine {

        public float start, end;
        public double angle, angleRadians;
        public Point2D.Double point1Rotated, point2Rotated;
        public BasicStroke stroke;
        public Paint paint;
        private AffineTransform transform;


        private ClockLine(float start, float end, double angle) {
            this(start, end, angle, 1);
        }

        private ClockLine(float start, float end, double angle, Paint paint) {
            this(start, end, angle, 1, paint);
        }

        private ClockLine(float start, float end, double angle, int thickness) {
            this(start, end, angle, new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), Color.black);
        }

        private ClockLine(float start, float end, double angle, int thickness, Paint paint) {
            this(start, end, angle, new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND), paint);
        }

        private ClockLine(float start, float end, double angle, BasicStroke stroke, Paint paint) {
            this.start = start;
            this.end = end;
            this.angle = angle;
            this.stroke = stroke;
            this.paint = paint;
            angleRadians = Math.toRadians(angle);
            transform = AffineTransform.getRotateInstance(angleRadians, center.getX(), center.getY());
            doRotation();
        }

        public Line2D.Double getLine() {
            return new Line2D.Double(point1Rotated, point2Rotated);
        }

        private void doRotation() {
            point1Rotated = new Point2D.Double(center.getX(), center.getY() - (getHeigth() / 2) * start);
            point2Rotated = new Point2D.Double(center.getX(), center.getY() - (getHeigth() / 2) * end);

            transform.transform(point1Rotated, point1Rotated);
            transform.transform(point2Rotated, point2Rotated);
        }
    }
}