/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.components;

import ch.jmnetwork.core.gui.event.JEvent;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

/**
 * User: joel / Date: 17.12.13 / Time: 19:00
 */
public class JButton extends JComponent {

    private Paint borderPaint = Color.black;
    private JButtonStyle style = JButtonStyle.RECTANGLE;
    private boolean enableBorder = true, clickedState = false;
    private String buttonText = "";

    /**
     * Simple constructor, calls {@link #JButton(int, int, int, int, String)} with empty button text
     *
     * @param x      Component x position
     * @param y      Component y position
     * @param width  Component width
     * @param heigth Component height
     */
    public JButton(int x, int y, int width, int heigth) {
        this(x, y, width, heigth, "");
    }

    /**
     * Extended constructor
     *
     * @param x       Component x position
     * @param y       Component y position
     * @param width   Component width
     * @param heigth  Component height
     * @param btnText Button text
     */
    public JButton(int x, int y, int width, int heigth, String btnText) {
        super(x, y, width, heigth);

        setBackgroundPaint(new GradientPaint(x / 2, 0, Color.gray, x / 2, heigth, Color.darkGray));
        setClickedBackgroundPaint(new GradientPaint(x / 2, 0, Color.darkGray, x / 2, heigth, Color.gray));
        setForegroundPaint(Color.black);

        buttonText = btnText;
    }

    /**
     * Enable / Disable the border around the button
     *
     * @param enable true = enable border / false = disable border
     */
    public void setBorderEnabled(boolean enable) {
        enableBorder = enable;
    }

    /**
     * Set the paint for the border
     *
     * @param paint The paint to set
     */
    public void setBorderPaint(Paint paint) {
        borderPaint = getRelativePaintIfNeccessary(paint);
    }

    /**
     * Sets the ButtonStyle
     * <p/>
     *
     * @param style
     */
    public void setButtonStyle(JButtonStyle style) {
        this.style = style;
    }

    /**
     * Sets the Text font using {@link #setComponentFont(java.awt.Font)}
     *
     * @param font The font to set
     */
    public void setTextFont(Font font) {
        setComponentFont(font);
    }

    /**
     * Sets the Text color using {@link #setForegroundPaint(java.awt.Paint)}
     *
     * @param color The color to set
     */
    public void setTextColor(Color color) {
        setForegroundPaint(color);
    }

    /**
     * @return The text currently displayed on the button
     */
    public String getText() {return buttonText;}

    /**
     * Sets the Text displayed on the button
     *
     * @param text the text to set
     */
    public void setText(String text) {
        buttonText = text;
        requestRepaint();
    }

    @Override
    public void draw(Graphics2D graphics) {
        graphics.setPaint(clickedState ? backgroundPaintClicked : backgroundPaint);
        graphics.setFont(componentFont);
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontMetrics fm = graphics.getFontMetrics();


        Shape buttonShape = null;
        switch (style) {
            case ROUND:
            case ROUND_EDGES:
                buttonShape = new RoundRectangle2D.Double(getX(), getY(), getWidth(), getHeigth(), 10, 10);
                break;
            case RECTANGLE:
            default:
                buttonShape = new Rectangle2D.Double(getX(), getY(), getWidth(), getHeigth());
        }

        graphics.fill(buttonShape);

        graphics.setPaint(foregroundPaint);
        int textX = (getX() + getWidth() / 2) - (fm.stringWidth(buttonText) / 2),
                textY = (getY() + (getHeigth() / 2)) + fm.getDescent();

        graphics.drawString(buttonText, textX, textY);
        if (enableBorder) {
            graphics.setPaint(borderPaint);
            graphics.draw(buttonShape);
        }
    }

    @Override
    public boolean takesEvent(JEvent.EventType type) {
        return type == JEvent.EventType.MOUSE_EVENT;
    }

    @Override
    public void onEvent(JEvent event) {
        MouseEvent event1 = event.getMouseEvent();
        JEvent.MouseEventType type = (JEvent.MouseEventType) event.specificType;

        if (clickedState != true && !(doesMouseEventApplyToMe(event1))) return;

        switch (type) {
            case CLICKED: // Handle click
                triggerEvent(event);
                break;
            case PRESSED: // Handle background
                clickedState = true;
                requestRepaint();
                break;
            case RELEASED: // Handle background
                clickedState = false;
                requestRepaint();
                break;
        }
    }

    /**
     * All the Buttonstyles available
     */
    public enum JButtonStyle {
        /**
         * Just a rectangle
         */
        RECTANGLE,

        /**
         * Rounded corners
         */
        ROUND_EDGES,

        /**
         * ATM the same as {@link JButton.JButtonStyle#ROUND_EDGES}
         */
        ROUND
    }
}
