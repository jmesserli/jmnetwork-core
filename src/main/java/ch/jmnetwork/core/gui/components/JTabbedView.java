/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.components;

import ch.jmnetwork.core.gui.JPage;
import ch.jmnetwork.core.gui.event.JEvent;
import ch.jmnetwork.core.gui.interfaces.IPage;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collections;

public class JTabbedView extends JComponent {

    private ArrayList<IPage> pages = new ArrayList<>();
    private ArrayList<Shape> tabAreas = new ArrayList<>();

    /**
     * Constructor
     *
     * @param x      the x coordinate of the Component
     * @param y      the y coordinate of the Component
     * @param width  the width of the Component
     * @param heigth the height of the Component
     */
    public JTabbedView(int x, int y, int width, int heigth) {
        super(x, y, width, heigth);
    }

    private void addToList(IPage page) {
        pages.add(page);
        Collections.sort(pages);
    }

    @Override
    public void draw(Graphics2D graphics) {
        debug(graphics);
    }

    private void debug(Graphics2D g) {
        Shape myShape = new Rectangle2D.Double(getX(), getY(), getWidth(), getHeigth());

        g.setPaint(Color.red);
        g.fill(myShape);

        if (pages.size() > 0) {
            pages.get(0).getPanel().draw(g);
        }
    }

    @Override
    public boolean takesEvent(JEvent.EventType type) {
        return type == JEvent.EventType.MOUSE_EVENT;
    }

    @Override
    public void onEvent(JEvent event) {

        MouseEvent mouseEvent = event.getMouseEvent();
        if (!doesMouseEventApplyToMe(mouseEvent)) return;

        // Check for clicks on Tabs
        for (Shape s : tabAreas) {
            if (s.contains(mouseEvent.getX(), mouseEvent.getY())) {
                // The shape of a tab contains the mouse click location

            }
        }
    }

    /**
     * Creates a new {@link ch.jmnetwork.core.gui.JPage}
     * <p/>
     * <b>This doesn't auto-add to the TabbedView!</b>
     *
     * @return A new {@link ch.jmnetwork.core.gui.JPage}
     * @see ch.jmnetwork.core.gui.JPage#JPage(String, JPanel)
     */
    public JPage newPage(String title, JPanel panel, int index) {
        panel.parent = this;
        return new JPage(title, panel, index);
    }

    /**
     * Adds a new page to the TabbedView
     *
     * @param title The title of the page
     * @param panel The {@link ch.jmnetwork.core.gui.components.JPanel} which contains all the Components of the new page
     */
    public void addPage(String title, JPanel panel) {
        panel.parent = this;
        addToList(new JPage(title, panel));
    }

    /**
     * Adds a new page to the TabbedView
     *
     * @param page The page to add
     */
    public void addPage(IPage page) {
        page.getPanel().parent = this;
        addToList(page);
    }
}
