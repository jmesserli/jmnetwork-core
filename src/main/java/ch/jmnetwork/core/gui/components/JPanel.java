/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.components;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

public class JPanel extends JComponent {

    private ArrayList<JComponent> components = new ArrayList<>();

    /**
     * Constructor
     *
     * @param x      the x coordinate of the Component
     * @param y      the y coordinate of the Component
     * @param width  the width of the Component
     * @param heigth the height of the Component
     */
    public JPanel(int x, int y, int width, int heigth) {
        super(x, y, width, heigth);
    }

    @Override
    public void draw(Graphics2D graphics) {
        Shape myShape = new Rectangle2D.Double(getX(), getY(), getWidth(), getHeigth());

        graphics.setPaint(backgroundPaint);
        graphics.fill(myShape);


        for (JComponent component : components) {
            component.draw(graphics);
        }
    }

    public void addComponent(JComponent component) {
        component.parent = this;
        components.add(component);
    }

    public void removeComponent(JComponent component) {
        if (components.contains(component)) {
            components.remove(component);
            component.parent = null;
        }
    }
}
