/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.components;

import ch.jmnetwork.core.gui.event.JEvent;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * User: joel / Date: 17.01.14 / Time: 16:51
 */
public class JInputField extends JComponent {

    public String text;
    private boolean focussed = false;

    /**
     * Constructor
     *
     * @param x      the x coordinate of the Component
     * @param y      the y coordinate of the Component
     * @param width  the width of the Component
     * @param heigth the height of the Component
     */
    public JInputField(int x, int y, int width, int heigth) {
        super(x, y, width, heigth);
        backgroundPaint = Color.gray;
        backgroundPaintClicked = Color.darkGray;
        foregroundPaint = Color.white;
    }

    @Override
    public void draw(Graphics2D g) {
        g.setFont(componentFont);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        FontMetrics fm = g.getFontMetrics();
        Shape bounds = myBounds();

        g.setPaint(backgroundPaint);
        g.fill(bounds);

        g.setPaint(foregroundPaint);
        g.drawString(text == null ? "" : text, getX() + 2, getY() + 2 + fm.getHeight());

        g.setPaint(focussed ? backgroundPaintClicked : backgroundPaint);
        g.setStroke(new BasicStroke(2));
        g.draw(bounds);
    }

    @Override
    public boolean takesEvent(JEvent.EventType type) {
        return true;
    }

    @Override
    public void onEvent(JEvent event) {
        switch (event.type) {
            case KEY_EVENT:
                KeyEvent e = event.getKeyEvent();
                if (focussed && event.specificType == JEvent.KeyEventType.TYPED) {
                    if (text != null) {
                        text += String.valueOf(e.getKeyChar());
                    } else {
                        text = String.valueOf(e.getKeyChar());
                    }
                } else if (focussed && event.specificType == JEvent.KeyEventType.RELEASED && e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                    if (text.length() > 1) text = text.substring(0, text.length() - 2);
                } else {
                    break;
                }
                requestRepaint();
                break;
            case MOUSE_EVENT:
                boolean focus_before = focussed;
                focussed = doesMouseEventApplyToMe(event.getMouseEvent());

                if (focus_before != focussed) requestRepaint();

                break;
        }
    }
}
