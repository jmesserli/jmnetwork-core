/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.components;

import ch.jmnetwork.core.gui.JGUIManager;
import ch.jmnetwork.core.gui.event.EventTriggerer;
import ch.jmnetwork.core.gui.event.JEvent;
import ch.jmnetwork.core.tools.gui.GuiUtils;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Every Component from the JMNetwork GUI stuff has to extend this baseclass
 * <p>
 * If you want to create your own Components which are compatible with JMNetwork GUI then you also have to extend this
 * <p>
 * <b>Notice: If you use {@link java.awt.GradientPaint}s, do not make them relative to the Buttons, this will be automatically done</b>
 *
 * @author Joel Messerli
 */
public class JComponent extends EventTriggerer {

    /**
     * The GUI Manager
     */
    public JGUIManager manager;
    /**
     * The Font which should be used for foreground Text
     */
    public Font componentFont = Font.getFont("Arial");
    /**
     * The foreground paint which should be used for e.g. Text that is in/on the Component
     */
    public Paint foregroundPaint;
    /**
     * The background paint which should be used for the Component's background.
     */
    public Paint backgroundPaint;
    /**
     * The background paint which should be used for the Component's background while a user is clicking the Component
     */
    public Paint backgroundPaintClicked;
    /**
     * The parent of this component, used for relative coordinates
     */
    public JComponent parent = null;
    /**
     * Coordinates / Sizes
     */
    private int x, y, width, heigth;

    /**
     * Constructor
     *
     * @param x      the x coordinate of the Component
     * @param y      the y coordinate of the Component
     * @param width  the width of the Component
     * @param heigth the height of the Component
     */
    public JComponent(int x, int y, int width, int heigth) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.heigth = heigth;
    }

    /**
     * Does the Component handle the specified {@link ch.jmnetwork.core.gui.event.JEvent.EventType}
     * <p>
     * Defaults to no event handled
     *
     * @param type The EventType
     * @return If the Component handles this event
     */
    public boolean takesEvent(JEvent.EventType type) {
        return false;
    }

    /**
     * Gets called when an event happens. Do your eventhandling in here
     *
     * @param event The event in form of a {@link ch.jmnetwork.core.gui.event.JEvent}
     */
    public void onEvent(JEvent event) {
        // Do some eventhandling!
    }

    /**
     * Gets called when the Component needs to be drawed. Do all your drawing in here.
     *
     * @param graphics The {@link java.awt.Graphics2D} object which you should use to draw your stuff
     */
    public void draw(Graphics2D graphics) {}

    /**
     * Prepares the graphics object for drawing
     *
     * @param graphics The {@link java.awt.Graphics2D} object which you use to draw your stuff
     */
    public void preDraw(Graphics2D graphics) {
        graphics.setFont(componentFont);
    }

    /**
     * Prepares the graphics object for foreground drawing
     *
     * @param graphics The {@link java.awt.Graphics2D} object which you use to draw your stuff
     */
    public void preDrawForeground(Graphics2D graphics) {
        preDraw(graphics);
        graphics.setPaint(foregroundPaint);
    }

    /**
     * Prepares the graphics object for background drawing
     *
     * @param graphics The {@link java.awt.Graphics2D} object which you use to draw your stuff
     */
    public void preDrawBackground(Graphics2D graphics) {
        preDraw(graphics);
        graphics.setPaint(backgroundPaint);
    }

    /**
     * Sets the GUI Manager for this Component, you don't need to call it, it gets called when a Component is added via the GUI Manager
     *
     * @param manager The {@link ch.jmnetwork.core.gui.JGUIManager} which manages this Component
     */
    public void init(JGUIManager manager) {
        this.manager = manager;
    }

    /**
     * Request a repaint from the manager, this will redraw the entire window using {@link javax.swing.JComponent}'s repaint.
     */
    public void requestRepaint() {
        manager.requestRepaint(this);
    }

    /**
     * Checks if the click of the {@link java.awt.event.MouseEvent} is on the component
     *
     * @param event The MouseEvent the component received
     * @return if the click is on the component
     */
    public boolean doesMouseEventApplyToMe(MouseEvent event) {
        return event.getX() >= getX() && event.getX() <= getX() + getWidth() && event.getY() >= getY() && event.getY() <= getY() + getHeigth();
    }

    /**
     * @param p
     * @return
     */
    public Paint getRelativePaintIfNeccessary(Paint p) {
        return p instanceof GradientPaint ? GuiUtils.getRelativeGradientPaint((GradientPaint) p, getX(), getY()) : p;
    }

    /**
     * Used to get the bounds of the component
     *
     * @return The bounds of this component
     */
    public Rectangle myBounds() {
        return new Rectangle(getX(), getY(), getWidth(), getHeigth());
    }

    /**
     * Sets the {@link #foregroundPaint}
     *
     * @param foregroundPaint The paint to set
     */
    public void setForegroundPaint(Paint foregroundPaint) {
        this.foregroundPaint = getRelativePaintIfNeccessary(foregroundPaint);
    }

    /**
     * Sets the {@link #backgroundPaint}
     *
     * @param backgroundPaint The paint to set
     */
    public void setBackgroundPaint(Paint backgroundPaint) {
        this.backgroundPaint = getRelativePaintIfNeccessary(backgroundPaint);
    }

    /**
     * Sets the {@link #backgroundPaintClicked}
     *
     * @param backgroundPaintClicked The paint to set
     */
    public void setClickedBackgroundPaint(Paint backgroundPaintClicked) {
        this.backgroundPaintClicked = getRelativePaintIfNeccessary(backgroundPaintClicked);
    }

    /**
     * Sets the {@link #componentFont}
     *
     * @param componentFont The font to set
     */
    public void setComponentFont(Font componentFont) {
        this.componentFont = componentFont;
    }

    public int getX() {
        return parent == null ? x : parent.getX() + x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return parent == null ? y : parent.getY() + y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeigth() {
        return heigth;
    }

    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

    @Override
    public String toString() {
        return "JComponent@" + Integer.toHexString(hashCode());
    }
}
