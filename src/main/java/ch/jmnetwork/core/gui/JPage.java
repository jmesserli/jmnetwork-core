/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui;

import ch.jmnetwork.core.gui.components.JPanel;
import ch.jmnetwork.core.gui.interfaces.IPage;

/**
 * Reference implementation of {@link ch.jmnetwork.core.gui.interfaces.IPage} use it or craft your own
 */
public class JPage implements IPage {

    private final JPanel panel;
    private final String title;
    private final int index;

    /**
     * @see ch.jmnetwork.core.gui.interfaces.IPage#getPanel()
     * @see ch.jmnetwork.core.gui.interfaces.IPage#getTitle()
     */
    public JPage(String title, JPanel panel) {
        this.title = title;
        this.panel = panel;
        this.index = 0;
    }

    /**
     * @see ch.jmnetwork.core.gui.interfaces.IPage#getPanel()
     * @see ch.jmnetwork.core.gui.interfaces.IPage#getTitle()
     * @see ch.jmnetwork.core.gui.interfaces.IPage#getIndex()
     */
    public JPage(String title, JPanel panel, int index) {
        this.panel = panel;
        this.title = title;
        this.index = index;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public JPanel getPanel() {
        return panel;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public int compareTo(IPage o) {
        return o.getIndex() > index ? -1 : o.getIndex() < index ? 1 : 0;
    }
}
