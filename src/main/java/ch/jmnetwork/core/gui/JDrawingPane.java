/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui;

import ch.jmnetwork.core.gui.event.JEvent;
import ch.jmnetwork.core.gui.interfaces.IDrawer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * The JComponent which is used to draw all the {@link ch.jmnetwork.core.gui.components.JComponent}s on it
 */
public class JDrawingPane extends JComponent implements IDrawer {

    JGUIManager manager;

    public JDrawingPane(JGUIManager manager) {
        this.manager = manager;
        manager.setDrawingPane(this);
        init();
    }

    @Override
    protected void paintComponent(Graphics g) {
        draw(g);
    }

    @Override
    public void init() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                manager.onEvent(new JEvent(JEvent.EventType.MOUSE_EVENT, JEvent.MouseEventType.CLICKED, e));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                manager.onEvent(new JEvent(JEvent.EventType.MOUSE_EVENT, JEvent.MouseEventType.PRESSED, e));
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                manager.onEvent(new JEvent(JEvent.EventType.MOUSE_EVENT, JEvent.MouseEventType.RELEASED, e));
            }
        });

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                manager.onEvent(new JEvent(JEvent.EventType.KEY_EVENT, JEvent.KeyEventType.TYPED, e));
            }

            @Override
            public void keyPressed(KeyEvent e) {
                manager.onEvent(new JEvent(JEvent.EventType.KEY_EVENT, JEvent.KeyEventType.PRESSED, e));
            }

            @Override
            public void keyReleased(KeyEvent e) {
                manager.onEvent(new JEvent(JEvent.EventType.KEY_EVENT, JEvent.KeyEventType.RELEASED, e));
            }
        });

        setFocusable(true);
    }

    @Override
    public void draw(Graphics graphics) {
        manager.draw(graphics);
    }

    @Override
    public Dimension getPreferredSize() {
        return manager.getSize();
    }

    /**
     * Get the manager which manages this JDrawingPane
     *
     * @return The JGUIManager thich manages this JDrawingPane
     */
    public JGUIManager getManager() {
        return manager;
    }
}
