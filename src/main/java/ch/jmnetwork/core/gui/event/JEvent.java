/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.event;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

/**
 * User: joel / Date: 02.01.14 / Time: 16:57
 */
public class JEvent {

    public EventType type;
    public SpecificType specificType;
    private MouseEvent mouseEvent;
    private KeyEvent keyEvent;

    public JEvent(EventType type, SpecificType specificType, Object event) {
        this.type = type;
        this.specificType = specificType;

        if (specificType instanceof MouseEventType) {
            mouseEvent = (MouseEvent) event;
        } else if (specificType instanceof KeyEventType) {
            keyEvent = (KeyEvent) event;
        }
    }

    public MouseEvent getMouseEvent() {
        return mouseEvent;
    }

    public KeyEvent getKeyEvent() {
        return keyEvent;
    }

    public interface SpecificType {
    }

    public enum EventType {
        MOUSE_EVENT, KEY_EVENT
    }

    public enum MouseEventType implements SpecificType {
        CLICKED, PRESSED, RELEASED
    }

    public enum KeyEventType implements SpecificType {
        TYPED, PRESSED, RELEASED
    }
}
