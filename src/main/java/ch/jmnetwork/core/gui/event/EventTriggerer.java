/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.gui.event;

import java.util.ArrayList;

/**
 * An {@link EventTriggerer} is an Object which can call events
 */
public class EventTriggerer {
    ArrayList<IEventListener> listeners = new ArrayList<>();

    /**
     * Add an {@link IEventListener} to the list of interested listeners
     *
     * @param listener The listener to add
     */
    public void addEventListener(IEventListener listener) {
        listeners.add(listener);
    }

    /**
     * Remove an {@link IEventListener} from the interested list.
     *
     * @param listener The listener to remove
     */
    public void removeEventListener(IEventListener listener) {
        listeners.remove(listener);
    }

    /**
     * Trigger an event and send it to all interessents
     *
     * @param arg The event to distribute
     */
    public void triggerEvent(JEvent arg) {
        for (IEventListener listener : listeners) listener.onEvent(arg);
    }
}
