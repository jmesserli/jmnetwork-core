/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.digitalocean;

public class Image {
    /**
     * The id of this image
     */
    public long id;
    /**
     * The human-readable name for this image
     */
    public String name;
    /**
     * The name of the distribution of this image
     */
    public String distribution;
    /**
     * The machine-readable name of this image
     */
    public String slug;
    /**
     * Wether or not this image is public
     */
    public boolean is_public;

    private DigitalOceanAPI api;

    public Image(long id, String name, String distribution, String slug, boolean is_public, DigitalOceanAPI api) {
        this.id = id;
        this.name = name;
        this.distribution = distribution;
        this.slug = slug;
        this.is_public = is_public;
        this.api = api;
    }
}
