/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.digitalocean;

import ch.jmnetwork.core.tools.network.RequestUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.ArrayList;

public class DigitalOceanAPI {
    private String api_key, client_id;

    public DigitalOceanAPI(String api_key, String client_id) {
        this.api_key = api_key;
        this.client_id = client_id;
    }

    private void checkStatus(JSONObject o) {
        String s = (String) o.get("status");
        if (!s.equals("OK"))
            throw new DigitalOceanException("Digitalocean API call status not OK! (Status = '" + s + "')");
    }

    public ArrayList<Droplet> getAllDroplets() {
        ArrayList<Droplet> droplets = new ArrayList<>();

        String response = RequestUtils.doGet("https://api.digitalocean.com/droplets/", "api_key", api_key, "client_id", client_id);

        Object res = JSONValue.parse(response);
        JSONObject array = (JSONObject) res;

        checkStatus(array);

        JSONArray dropletArray = (JSONArray) array.get("droplets");

        for (Object o : dropletArray) {
            JSONObject myobj = (JSONObject) o;

            droplets.add(new Droplet((long) myobj.get("id"), (long) myobj.get("image_id"), (long) myobj.get("size_id"), (long) myobj.get("region_id"), (String) myobj.get("name"), (String) myobj.get("ip_address"), (String) myobj.get("private_ip_address"), (String) myobj.get("status"), (String) myobj.get("created_at"), (boolean) myobj.get("backups_active"), (boolean) myobj.get("locked"), this));
        }

        return droplets;
    }

    public ArrayList<Region> getAllRegions() {
        ArrayList<Region> regions = new ArrayList<>();

        String response = RequestUtils.doGet("https://api.digitalocean.com/regions/", "api_key", api_key, "client_id", client_id);

        JSONObject object = (JSONObject) JSONValue.parse(response);

        checkStatus(object);

        JSONArray regionArray = (JSONArray) object.get("regions");

        for (Object o : regionArray) {
            JSONObject obj = (JSONObject) o;

            regions.add(new Region((long) obj.get("id"), (String) obj.get("name"), (String) obj.get("slug")));
        }

        return regions;
    }

    public ArrayList<Image> getAllImages() {
        ArrayList<Image> images = new ArrayList<>();

        String response = RequestUtils.doGet("https://api.digitalocean.com/images/", "api_key", api_key, "client_id", client_id);

        JSONObject object = (JSONObject) JSONValue.parse(response);

        checkStatus(object);

        JSONArray imageArray = (JSONArray) object.get("images");

        for (Object o : imageArray) {
            JSONObject obj = (JSONObject) o;

            images.add(new Image((long) obj.get("id"), (String) obj.get("name"), (String) obj.get("distribution"), (String) obj.get("slug"), (boolean) obj.get("public"), this));
        }

        return images;
    }

    public ArrayList<Size> getAllSizes() {
        ArrayList<Size> sizes = new ArrayList<>();

        String response = RequestUtils.doGet("https://api.digitalocean.com/sizes/", "api_key", api_key, "client_id", client_id);

        JSONObject object = (JSONObject) JSONValue.parse(response);

        checkStatus(object);

        JSONArray sizeArray = (JSONArray) object.get("sizes");

        for (Object o : sizeArray) {
            JSONObject obj = (JSONObject) o;

            sizes.add(new Size((long) obj.get("id"), (String) obj.get("name"), (String) obj.get("slug")));
        }

        return sizes;
    }

    public class DigitalOceanException extends RuntimeException {

        DigitalOceanException(String s) {
            super(s);
        }
    }
}
