/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.digitalocean;

import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.logging.LogLevel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Droplet {
    private static SimpleDateFormat created_dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    /**
     * The id of this droplet
     */
    public long id;
    /**
     * The id of the image of this droplet
     */
    public long image_id;
    /**
     * The id of the size this droplet has ("Plan")
     */
    public long size_id;
    /**
     * The id of the region where this droplet is hosted
     */
    public long region_id;
    /**
     * The user-defined name of this droplet
     */
    public String name;
    /**
     * The public ip-address of this droplet
     */
    public String ip_address;
    /**
     * The private ip-address of this droplet
     */
    public String private_ip_address;
    /**
     * The status of this droplet
     */
    public String status;
    /**
     * The date this droplet was created
     * </p>
     * yyyy-MM-dd'T'HH:mm:ss'Z'
     */
    public String created_at;
    /**
     * Whether or not backups are activated
     */
    public boolean backups_active;
    /**
     * Whether or not this droplet is locked
     */
    public boolean locked;

    private DigitalOceanAPI api;

    public Droplet(long id, long image_id, long size_id, long region_id, String name, String ip_address, String private_ip_address, String status, String created_at, boolean backups_active, boolean locked, DigitalOceanAPI api) {
        this.id = id;
        this.image_id = image_id;
        this.size_id = size_id;
        this.region_id = region_id;
        this.name = name;
        this.ip_address = ip_address;
        this.private_ip_address = private_ip_address;
        this.status = status;
        this.created_at = created_at;
        this.backups_active = backups_active;
        this.locked = locked;
        this.api = api;
    }

    public Date getDateCreated() {
        try {
            return created_dateformat.parse(created_at);
        } catch (ParseException e) {
            JMNetworkCore.logger.logStackTrace(LogLevel.ERROR, e);
        }
        return null;
    }
}
