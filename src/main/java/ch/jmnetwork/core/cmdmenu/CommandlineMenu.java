/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.cmdmenu;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class CommandlineMenu {

    private boolean doMenuLoop = true;
    private ArrayList<MenuEntry> entries = new ArrayList<>();
    private HashMap<Integer, Integer> indexMap = new HashMap<>();
    private String title;

    public CommandlineMenu(String title) {
        this.title = title;
    }

    private void updateIndexMap() {
        indexMap = new HashMap<>();

        int _cnt = 0, cnt = 1;
        for (MenuEntry e : entries) {
            boolean isSpacer = e instanceof SpacerEntry;

            if (isSpacer) {
                _cnt++;
            } else {
                indexMap.put(cnt, _cnt);
                cnt++;
                _cnt++;
            }
        }
    }

    public void resetEntries() {
        entries = new ArrayList<>();
    }

    public void rebuildEntries() {}

    public void addEntry(MenuEntry entry) {
        entries.add(entry);
        updateIndexMap();
    }

    public void addEntryAt(MenuEntry entry, int index) {
        entries.add(index, entry);
    }

    public MenuEntry getEntryAt(int index) {
        return entries.get(index);
    }

    public void printMappings(PrintStream o) {
        for (Integer i : indexMap.keySet()) {
            o.println(i + " -> " + indexMap.get(i));
        }
    }

    public void printMenu(PrintStream o) {
        o.println(title + "\n");
        int _cnt = 1;
        for (MenuEntry e : entries) {
            if (e instanceof SpacerEntry) {
                o.println();
                continue;
            }
            o.println(_cnt + ": " + e.name);
            _cnt++;
        }
    }

    public void getAndHandleInput(PrintStream o) {
        o.print("> ");
        int input = new Scanner(System.in).nextInt();

        if (!indexMap.containsKey(input)) return;

        int index = indexMap.get(input);
        MenuEntry entry = getEntryAt(index);

        doCall(entry, input - 1, this);
    }

    public void doCall(MenuEntry toCall, int index, CommandlineMenu menu) {
        toCall.call(index, menu);
    }

    public void menuLoop() {
        doMenuLoop = true;
        while (doMenuLoop) {
            printMenu(System.out);
            getAndHandleInput(System.out);
        }
    }

    public void stopMenuLoop() {
        doMenuLoop = false;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
