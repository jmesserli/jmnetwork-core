/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.cmdmenu;

public class MenuEntry {

    public String name;
    private EntryAction myAction;

    public MenuEntry(String name, EntryAction action) {
        this.name = name;
        myAction = action;
    }

    public EntryAction getAction() {
        return myAction;
    }

    public void call(int myIndex, CommandlineMenu commandlineMenu) {
        myAction.call(myIndex, commandlineMenu);
    }
}
