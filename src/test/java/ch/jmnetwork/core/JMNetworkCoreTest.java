/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JMNetworkCoreTest {

    @Test
    public void testGetCoreVersion() throws Exception {
        assertEquals("Reference Version " + CoreReference.VERSION + " should be the same as JMNetworkCore.getCoreVersion()", CoreReference.VERSION, JMNetworkCore.getCoreVersion());
    }

    @Test
    public void testLoggerInstance() throws Exception {
        assertNotNull("Logger should not be null!", JMNetworkCore.logger);
    }
}
