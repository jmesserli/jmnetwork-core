/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.iterate;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * User: joel / Date: 24.05.14 / Time: 11:35
 */
public class RunnerTest {
    int times = 10;
    int timesRun;
    Runner r;

    @Before
    public void setUp() throws Exception {
        timesRun = 0;
        r = new Runner(times) {
            @Override
            protected void doRun() {
                timesRun++;
            }
        };
    }

    @Test
    public void testRun() throws Exception {
        r.run();
        assertEquals(times, timesRun);
    }
}
