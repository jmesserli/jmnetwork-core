/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.reflection;

import ch.jmnetwork.core.CoreReference;
import ch.jmnetwork.core.JMNetworkCore;
import ch.jmnetwork.core.annotations.Info;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ReflectionUtilsTest {
    @Test
    public void testGetClassAnnotationValue() throws Exception {
        assertEquals(CoreReference.VERSION, ReflectionUtils.getClassAnnotationValue(JMNetworkCore.class, Info.class, "version"));
    }

    @Test
    public void testParseModifiers() throws Exception {
        assertEquals("public", ReflectionUtils.getModifierStringFrom(ReflectionUtils.class));
    }
}
