/*
 # JMNetworkCore Copyright (C) 2014 Joel Messerli - JMNetwork.ch
 # -------------------------------------------------------------------
 #
 # This program is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with this program.  If not, see [http://www.gnu.org/licenses/gpl-3.0.html].
 #################################################################################################*/

package ch.jmnetwork.core.tools.array;

import ch.jmnetwork.core.tools.code.Functions;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ArrayUtilsTest {
    ArrayList<String> strings;
    String[] stringArray;

    @Before
    public void setUp() throws Exception {
        strings = new ArrayList<>();
        strings.add("Swag");
        strings.add("Swaggier");

        stringArray = new String[]{"Swag", "Swaggier"};
    }

    @Test
    public void testArrayToList() throws Exception {
        ArrayList arrToList = ArrayUtils.arrayToList(stringArray);
        assertEquals("Size of the list must be the same as the length of the array!", stringArray.length, arrToList.size());
        assertTrue("List must contain all elements of the array!", arrToList.contains(stringArray[0]) && arrToList.contains(stringArray[1]));
    }

    @Test
    public void testJoin() throws Exception {
        assertEquals("Swag, Swaggier", ArrayUtils.join(", ", Functions.OBJECT_TO_STRING, stringArray));
    }

    @Test
    public void testContains() throws Exception {
        assertTrue(ArrayUtils.contains(stringArray, "Swag") && ArrayUtils.contains(stringArray, "Swaggier"));
    }

    @Test
    public void testArrayWithout() throws Exception {
        String[] newArray = ArrayUtils.arrayWithout(stringArray, String.class, 0);
        assertTrue(newArray.length == stringArray.length - 1);
        assertEquals("Swaggier", newArray[0]);
    }
}
